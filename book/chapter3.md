+++
title = "Chapter 3: Breaking the Chains - Hack Parasitic Programmes, Dismantle Your Mind Implants"
description = "To dismantle the great magic and its formulas, you and I will log into the secret portal of the alchemy network."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

To dismantle the great magic and its formulas, you and I will log into the secret portal of the alchemy network.

<!-- more -->

---

## The Four Basic Elements, Failed Transmutations, and the Struggles of Life

Prima materia, dismantling the four basic elements that maintain the status quo in order to obtain the first matter. What does it all mean?

This time I will share the answer first. It is a spectacular Gestalt collapse. How spectacular? It is a dismantling of the hidden formulas and a renewal of the framework, with the power to destroy a planetary civilisation from the ground up.

In many cases, in magic and alchemy, the four basic elements have been revered and venerated as essential elements for the success of the art. Have not the elements been placed in the same category as spirits and gods, associated with the guardians of the four winds, and even exercised summoning techniques?

Here you must be wondering. Yes, the four basic elements are supposed to be 'good'. Not the 'evil' that maintains the status quo of suffering. But I want you to put aside your prejudices and think deeper. Think about the problems in this world that humanity has created so far. The reasons why life has gone wrong. And the real reason why your magic and my alchemy have failed many times.

I want to clarify the root cause here.

This problem can never be brushed aside with emotionalism, such as blame or self-responsibility, the fault of society or government, or a conspiracy of secret societies or world government. Furthermore, it is not a modern approach that throws all vague phenomena and events whose causes are not well understood into the realm of the subconscious or unconscious minds. Nor is it a fairy tale in which the blame can be shifted to God, the devil, angels or Lilith.

We are magicians and alchemists. So we want to use our intuition and logical mind more fully to identify the cause. As rare professionals, inheriting the legacy of a very ancient civilisation of consciousness and energy technology, we want to analyse the problem systematically and find the best solution. Otherwise, efforts are not reproducible and we are faced with the same problems over and over again. So history repeats itself.

The repetitive phenomenon is perceived as karma, where similar problems appear again and again in life in exactly the same pattern. It would seem as if someone is running a fractal, topological programme on a hidden schedule, and it would seem as if an 'event-driven' programme is automatically running, triggered in areas we cannot recognise, and which is sure to cause disturbances and attacks.

Many people today would just brush it off as subconscious or unconscious minds. This is not the optimal solution. Because while the approach may be suitable in the abstract, it is too vague to pinpoint the problem. In other words, it cannot be used as a technology in this state.

It is like pretending that an all-encompassing plan is a strategy when planning a new business, or setting the target segment for marketing to SMEs across the country, or simply playing at calling a marketing initiative a strategy. It is likely to end up being a wasteful delusion, not a technology.

This is why the noticeboards of the alchemist network, which can only be accessed by the onion router Tor and seem to exist on the dark web (it's metaphor), carry advice akin to a warning to "Be careful of reading books written by Jung and Kant".

As a side note, this secret alchemist network can be joined by accessing the Emerald Tablet with the correct protocols. Alternatively, connect to the portal when meditating on the illustrations of the alchemical books. Of course, access is also possible from the Sephiroth. However, as mentioned earlier, a pair of public and private keys, and a passphrase are also required.

To return to the subject, when the four basic elements are re-conceptualised as part of the technology, their true nature comes into view. When you get down to it, it is, of course, a state of pure consciousness, but a different aspect must have come into view. Yes, the four basic elements serve as a kind of programme and code conditioning consciousness and energy. That is the root cause of the problem.

In other words, the way we use and condition the four basic elements has a huge impact on our lives and world affairs. There are no categories of good and evil. Rather, the categories of good and evil are also related to the conditioning, or implementation, of the four basic elements. Thus, potential energy is transformed into the vector of good and evil and flows through the system that operates according to the system specification of the principle of polarity.

When we analyse the system in this way, we realise that dismantling the four basic elements of the status quo is equal to dismantling the planetary civilisation from its very foundations. On this planet, it is a revolution that will cause a gestalt collapse on the level of dismantling the platform of global society. This is not a metaphor, but the same applies to the energy problem.

Hence, it has been forbidden to reveal this secret to the ignorant and selfish. On the contrary, it is a wisdom that has been kept secret by the real rulers in order to maintain their vested interests. Now that the planetary energies are shifting, and evil is being exposed in the light of day, and our backroom business is coming out into the public, you can see that it's all for the same cause.

And whatever the purpose, if we don't know about the four basic elements in depth before we start to refine a certain thing, our refinement will fail with a high probability. Neither innovation nor revolution will succeed. Only when we boil down to this level can we say that we are 'hacking consciousness'.

## Hack the Four Basic Elements and System Specifications

So far, we have prepared ourselves for hacking consciousness, a process akin to data pre-processing in AI applications. Yes, we have begun to understand the technology and its basic system specifications on how to channel the raw data of pure energy.

It is so generic or primitive that you may have felt overwhelmed. In other words, it is a technology that can be implemented in any way we can think of. Humanity that is unable to use such highly flexible systems and technologies, and continues to take from others, is no better than a primitive organism kept on a cursed magic platform. That is the very truth of this world.

That is why we must use this technology to create a huge gestalt collapse.

This foundation is important for the betterment of life and the world. As we have discussed together, having this knowledge makes a huge difference. If we were to express the difference in terms of business, it would be as different as an SME that disguises its strategy as an all-encompassing plan based on government statistical data and Nikkei newspaper articles, and a start-up that pinpoints and plugs solutions to problems before starting to diversify its management.

So, once again, I would like to redefine what the four basic elements are, as systems and technologies.

We are magicians and alchemists living in the modern world. So here, I'm going to ask the latest search engine-linked AI to summarise what the four basic elements are and the mental models of humanity. The AI's summary, drawn from online search results, is this.

> Alchemy and magic are based on the concept of the four basic elements: earth, water, air and fire. These elements represent the fundamental building blocks of the universe and are often used to metaphorically express aspects of human nature and human experience. In alchemy in particular, the four basic elements are considered to be the basic building blocks of all matter, and processes such as metamorphosis aim to transform one element into another. In magic, the four basic elements are often associated with different directions or reference points (e.g. earth to the north, air to the east, fire to the south and water to the west) and are used in rituals and spells to amplify or balance different energies. The concept of the four basic elements has a long history of human thought and has been interpreted in different ways by different cultures and traditions.

Maybe you have succeeded in hacking into the collective consciousness at this point. Or you may have had direct access to the programmes and platforms of the 'genetic mind' at the base of humanity's collective consciousness. That is how impactful this summary is.

Before we go deeper into analysis and hacking, we would also like to access the secret alchemist network here. To connect to the database of wisdom, which has been secretly but continuously passed down since ancient times, we shall access it from the 'illustration' as declared.

The alchemical book 'Splendor Solis' from the 1500s will be used as a portal. This alchemical book is thought to cover the process of transmutation the Philosopher's Stone and the Hermetic philosophy, and is a high quality application that has been studied by many alchemists throughout history. It is therefore the perfect material for accessing the profound area as a portal, an endpoint for network access.

The portal on the four basic elements can be entered from an endpoint where a wise man in red and blue-purple clothing is examining a flask. There, in Latin, it is written.

*'Eamus quefitum quafuor elementorum naturas'.*

This translates into English as 'Let us explore the nature of the four basic elements', which in Japanese translates into the advice 'Examine the nature of the four basic elements'. It is important to integrate this illustration with the text (and the overall context) and recognise it as a hidden login screen to the alchemist network.

Are we ready?

There are several points to consider when joining the network. First, the Latin advice is written on a smoky black background. The flask represents a philosopher's egg. And that the wise man's clothes probably mean the 2 polarities of the light spectrum.

Furthermore, it is significant that the wise man is trying to investigate the four basic elements in the secluded and quiet 'book of nature'. It is also significant that the portal is surrounded by objects that suggest the plant and animal worlds and, if the ground is included, the mineral world.

Have you recognised these elements and objects? If you have realised that this 'Splendor Solis' is itself a replica of the Book of Nature, then you are already logged in. All that remains is to realise that the philosopher's egg needs "chaotic primordial pure energy", which is why it is represented in black, and you are ready. This can be easily understood as the image of black when all the CMYKs are mixed together.

In other words, investigating the nature of the four basic elements - earth, fire, water and air - is equal to investigating the results of energy flow, mindset programmes, implemented mental models, interactions with the environment and complex feedback loops.

As expressed by magicians and alchemists, this means 'discovering the hidden formula' in the sample material of the things that make up this world.

The hidden art formula is made by combining the properties that have been handed down as the four and five elements. The first step is to discover them, understand their behaviour and specifications, dismantle them, purify them down to the first matter, the prima materia, and create reality from them.

## Dismantling the Magic Formula of Hidden Mind Implants

Using this secret approach, it is of course possible, albeit with a high degree of difficulty, to dismantle the problems and karma that persist in our lives. It is often caused by the hidden formulas that we and others have implemented, extended and maintained in one form or another.

It is a local artefact, not a principle of the universe as it is referred to in the spiritual industry. Nevertheless, it is also true that some of the great magic that has been applied on a solar system scale seems to be a principle. Their scale and esoteric nature are the reason why many business spiritualists and spiritual enthusiasts mistake them for universal laws of the universe.

However, the principles of such grand magic and 'magnum opus' are the same as the small magic and personal cultivation we practise, and the basic specifications of implementation are exactly the same. That is why the magic and alchemy of you and me has the power not only to improve individual lives, but also to overturn the planetary civilisation from the ground up.

To use a modern technology analogy, building a server at home and developing a web application on it is personal small-scale magic and microcosmic alchemy. Improving the same application and deploying it in a large-scale cloud environment is large-scale magic or macrocosmic training.

However, there are also many magics in this world that reduce our power, seal our abilities and obstruct our actions. Therefore, when we observe the world as a 'book of nature', we begin to realise that there are obstruction black magics hidden in every single thing. And some of them are event-driven, with triggers that fire automatically. This is the benefit of reading the book of nature.

Its black magic formulas are fractal and topological, both on a large scale and on a small scale. This is because it is a multiple inheritance of basic magic formulas using the plain four basic elements and pure first matter, like matryoshka dolls. It is transparent as multiple applications, developed on a specific framework, in modern technology.

For those familiar with virtual environments, it is similar to a virtual host running on a physical server, with many virtual machines running on the virtual host operating system. The planetary civilisation and its jamming techniques are implemented as a logical resource pool that brings together multiple physical servers. Reincarnation is therefore perceived as the automatic stopping, starting and terminating of virtual machines. The dismantling of this interesting and very well-designed grand magic will be left for another time.

As we dismantle this large-scale art installed in the human mind, something becomes clear. That is, if our mission is to change the world, we must discover the conditioning of the four basic elements that make up this world and planetary civilisation, and the 'core' of the programmes that have been implemented.

That means understanding the business rules of systems development, identifying the goals of the system, understanding the infrastructure design, reverse engineering the app, and deploying tactics linked to strategic thinking to discover and dismantle problematic programmes, code and hopefully even their hidden concepts - literally, it is the work of 'ethical hacker groups'. That is the mission of the magicians and alchemists.

The information on the four basic elements summarised by the AI also actually reveals this multiple inheritance. It is, for example, the connection between the direction (the four winds) and the four basic elements. This can also be seen in the sense of implementing a model, which can be seen as like implementing an interface. In other words, the relationship between direction and the four basic elements is a local artefact, not a universal law or fixed specification in this universe or galaxy.

Therefore, if you want to realise what magicians call the 'true will', the true intention emanating from the depths of your consciousness, you must thoroughly dismantle the techniques of planetary civilisation, hack the genetic mind, and you have to keep working on your own 'human device', including the physical body and genetic information, over and over again so that you can influence the world as a Philosopher's Stone.

Interestingly, with regard to hacking and upgrading the human device, it is the design of the alchemist's athanor, the furnace in the Taoist temple, that suggests this in a straightforward manner. This design and system structure is also consistent with the 'structure of the universe' of Hermetic thought. It is truly a fractal structure.

And here is a shocking fact. The 'structure of the universe' as conceived by the Hermetists is the very black magic formula that targets us, the human race. That is the core of the system.

And the civilisation of this planet has been established by multiple inheritance of that base formula and system structure. And it is the true nature of the platform that has been secretly enhanced and operated by forces wishing to maintain their vested interests. The details of this can be found in books on Hermetic philosophy, so I will not go into them here.

However, one thing can be said. That is that between the archetype of Jehovah in the heavens and the chaos of Satan in the underground, the four basic elements (including the fifth element to be precise) exist, and that the conditioning and programming of the human race (the human device) represented by Homo is the system structure of the "ruling art" itself.

And it is defined according to the structure of the human device, as you know from the Kundalini Awakening and the Caduceus identity. It is a system that has followed the essence and structure of the universe, but distorted and replaced it. Therefore, the magician who believes in this 'structure of the universe' as a universal law that pervades everything is a captive magician, and the alchemist who can strategically use it as the basis for a hackable civilisation, can enter into an alchemy that will overturn the world from its very foundations.

So, too, is the microcosm. Recall what the Hermetists call the 'principle of correspondence'. It is the 'alchemy of form' that refers to the resonance between the macrocosm and the microcosm. You and I have already used the alchemy of form through this record and have obtained the secret of the dismantling of the formula. Therefore, from now on, I would like to discuss an approach to dismantle the invisible implants placed in the individual mind.

The individual mind and the genetic mind. This can generally be viewed in terms of the relationship between the conscious and subconscious/unconscious minds. However, it is not enough. The first thing that needs to be understood is the basic specification that the consciousness in the human device is different from the consciousness from which it is borrowed.

Once this is recognised, our consciousness can perceive things as objective, an extension of our subjectivity, and the genetic network and database of the human device, i.e. our physical body. This is what is practised as an effort to separate facts from feelings and to look ourselves objectively as much as possibly we can.

If you try to achieve it by willpower or guts, you will fail in the alchemy. However, if one tries to realise it as a technology after grasping the system specifications and information structure, things will proceed more effortlessly. Furthermore, by carrying out transmutation from a pure state, without inheriting the 'structure of the universe, the truth of the world' that is held as common knowledge by the collective consciousness of humanity, it is possible to avoid inheriting someone else's magic platform.

This is the art dismantling that has been implied by alchemists who have succeeded in true alchemy as an 'escape from the perceiving reality', which leads to the dismantling of the mind's implants, which have been reinforced by heredity, history, culture and lifestyle.

What are mind implants really? It is diverse and manifold, so it is not a uniform or homogenous device, but this much can be said. It is recognised in the form of common perceptions, group psychology, belief systems, personal assumptions and behaviour patterns, such as 'this is how it is, therefore this is how it will be'.

Of course, there are genetic implants inherited from ancestors and parents, as well as very hard and nasty models fostered by society and groups. Many of these intertwine to make the black magic formula a powerful hologram reality, which has blocked your magic and mine many times.

Therefore, we must be strategic and patient in tackling the large number of techniques that form the basis of these holograms. It is, in modern terms, an implementable model, one that is provided as a framework. The philosopher's stone can be obtained by daring not to adopt the existing systems and models by thoroughly purifying and dismantling them.
