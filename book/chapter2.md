+++
title = "Chapter 2: Discovery of Prima Materia, and the Public Key to Decipher"
description = "To find out what the prima materia really is, you and I will travel beyond space-time using an ancient time-travelling technology."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

To find out what the prima materia really is, you and I will travel beyond space-time using an ancient time-travelling technology.

<!-- more -->

---

## The Mystery Surrounding Prima Materia

The first matter, the prima materia. The stuff that all alchemists have been searching for for centuries. The material needed to prepare the philosopher's egg. The material without which the transformation of matter and the smelting of metals are impossible. Conversely, a mysterious material without which it is said to be possible to begin work.

And although the mysterious material is available to everyone, it is said that everyone ignores it and treats it carelessly. Some say it is water, some say it is world spirit, some say it is chi. Some New Age people seem to equate it with ether and plasma.

No one really knows, and those who have succeeded in refining it have remained tight-lipped and have left this world.

But there are clues. It is a matter of broadening our horizons a little and looking for information in closer fields. Kabbalah, for example. Alchemy and Kabbalah have had a close relationship, and for someone like me, who was drilled in Christian doctrine from an early age, there should be a reasonable chance of deciphering it. There is even a category of Christian Kabbalah.

According to extant sources and folklore, the Kabbalah is said to be a kind of remnant from the time of Atlantis. It is also thought to contain clues to the technology of super-ancient civilisations. This means that it must be related to the so-called 'lost arks' or 'lost words' that secret societies are looking for. Apparently, if we continue with alchemy, we may be able to manifest civilisations from other worlds in the present day.

## The Public Key to Open the Portal

A little off topic, but an important mystery is being recorded here. The key to finding for the lost language is symbols and mental imagery. This is evident in secret society buildings filled with symbols. The same goes for the magic circles and ancient scripts often seen in anime. Runes and Katakamuna are also symbols. When I was a member of a group that inherited ancient Egyptian magic, all sorts of symbols were used in group rituals.

When you read alchemical books and meditate on their illustrations, you are trying to connect to an alchemical network that transcends linear space-time. The 'TCP/IP handshake' in network communication has been established, so all that remains is to pass the security of the application layer running on top of the protocol, and you can join the secret alchemical network. To do this, you need keys.

The keys will always be with you on your 'alchemical journey' that is about to begin. Therefore, I would like to give you the 'public key' to solve the mystery here in advance. Yes, to open the door to the invisible world, you need a pair of private and public keys. It's like SSH key authentication. A passphrase is a spell or chant. The identity of the private key will be documented at another time.

The key to the lost language is not so much a specific language, but vibes. Yes, it has to do with vibrations. There is an important reason why I have ventured to use vibes rather than waves or frequencies. It is to distinguish it from the misconceptions and misdirection that the spiritual industry has fed on.

When we alchemists talk about vibes, we want you to understand that it is a method of communication that treats sound and light as a multidimensional network communication. It is like manipulating sound waves while dealing with electromagnetic waves. It is practised through actual actions and rituals that pass energy through the technique.

I think you know what I mean. The lost language is 'movement'. It is a kind of incantation, using a language that is understood by all things in the universe. The clue is motion, translated in a modern way. A common language that can be understood at the quantum and genetic levels, in the mineral, plant and animal kingdoms, as well as by us humans and the four basic elements. It is indeed motion. This is the basic specification of the universe, which is gradually understood as you begin to practise small magic.

When movement is perceived, it becomes vibes. Vibes create an atmosphere. The word atmosphere can also mean aura or climate. Ambient music is also about atmosphere. So the key to the lost language is vibes, and we will be using this key again and again.

The astute among you have already discovered that the correct use of the vibes, the key to the lost language, makes a huge difference to the power and effectiveness of the magic and alchemy we practise. What's more, the secret key you already have is also deeply connected to the vibes. And another top secret prospect is that even lost ancient arks could be opened with this pair of public and private keys.

## The Time Travel Technique of Alchemists

Now we are going to travel back in time to the distant past. We want to explore the past first, as the clues seem to lie in Atlantis and other super-ancient civilisations. However, neither you nor I have access to such advanced technology out of the blue, so we're going to use the least expensive method this time.

That is the technique of sending your consciousness back in time. We don't want to make the mistake of picking up memories in our heads or mental information from our environment and feeling like we've jumped into the past. So, if possible, keep books and devices away from your environment, close browser tabs and reduce the amount of miscellaneous information that your lower (subconscious) mind picks up.

This is the time travel through pure consciousness. To be precise, it's not so much about flying consciousness as it is about tuning into and remotely resonating with a similar form of consciousness in the past. So we don't have to fly or dismantle our physical bodies, we don't need astral projection or the ark.

You and I communicate telepathically, resonating our consciousness with the past. Telepathy is not Japanese or English, but quantum teleportation of a philosopher's egg of a very high information density. The philosopher's egg unfolds as a hologram, allowing you and me to share our minds. This technique can also be used to create and deploy holograms of linguistic information, such as this document.

The internet analogy is similar to chatting while videoconferencing on Zoom, or communicating on Slack or Discord while watching a video. We should be able to do this with only consciousness.

We've gone a bit far, but we've been able to share a public key called vibes. And through this document, we are also able to share the communication specifications for sharing the mind between each other at the same time.

Remote resonance with a similar form of consciousness. In other words, we have prepared ourselves to communicate through the alchemy of form and the philosopher's egg.

In other words, this is a psychic otherworldly experience that does not require a headset or a metaverse. If you are reading this transcript now and three-dimensional images, information structures and relationships between things are unfolding in your mind, psychic communication is already taking place. It is a matter of attuning your consciousness to distant fractal objects.

As you strengthen this basic technique, your consciousness will resonate with the past. By tuning into this record, we are exploring the past as an extension of establishing telepathic protocols. It's like a server administrator logging into the cloud via SSH and updating multiple servers from there.

That's what this document does. That's why they can only work with people like you, who have a special secret key. I think you get the idea that it's this super-personal, secret technology. This is all an area that is beyond the reach of today's artificial intelligence based on statistical probability.

With this supernatural consciousness technology, our journey of refinement literally begins beyond linear time and space. And this time, the distant past that we are remotely resonating with is the so-called 'Golden Age', when super-ancient civilisations flourished. Yes, the goal is the Golden Age of this planet Terra.

## Exploration and Discovery of Prima Materia

At that time, Japan was still connected to the continent, and I think there was a bigger land area around India than there is now. And we knew better than now what the first matter was. I mean, I don't think there were any hidden secrets, because alchemy was deeply integrated into the technology of the civilisation at that time.

Still, alchemy involving energy was risky, so scientists working on the latest technology and energy management centres were closely guarded and isolated from the public. Anyone could benefit from it 'for free', and the technology to efficiently collect and convert energy was widely known. But security was tightly controlled.

This was also to prevent accidents by those who did not know how to use it. Even today, it is still very bad practice for amateurs to freely enter and leave nuclear power plants or to transport plutonium in the boot of a car. It is the same. But the greater risks feared were the development of weapons, conflicts of interest and war with other species.

It is clear that the history of this planet, from the very ancient times to the present, is a history of conflicts of interest. This is something that alchemists and magicians will understand if they look closely at the current energy wars, information wars, the struggle for the powerful energy resource of human consciousness, business and markets, the mindset of CEOs, and so on.

We are creatures that project and express our genetic settings, or programmes, directly onto the outside world. The expression of conflict, which has been going on since super-ancient times, is still firmly in place in the present. For this reason, the first matter and its handling has been very sensitive. The reality is that, as we know from resonating with the ancient consciousness, some civilisations have been destroyed by it, disrupting the magnetic grid of the planet and causing the pole shift.

In the past, remote sensing of the area known as the Astral Realm showed that there was a civilisation beneath the planet. It could be seen that there were races different from our human species, and they were fighting fiercely. I've been in danger a few times. In other words, the history of the planet, all the other worlds on the planet, and this reality that we perceive, have all been caused by alchemy and magic. And it was largely due to the first matter and its handling.

Have you ever heard of a French alchemist called Fulcanelli? He is a mysterious figure, who is said to have been the master of the master Canseliet, and his books revealing the secrets of Notre Dame Cathedral are well known. Rumour has it that Fulcanelli was also involved in an energy-related conflict of interest, which is why he disappeared.

Let us return to our focus on time travel and super-ancient civilisations.

As I have written before in this document, alchemy is 'a technology that deals with consciousness and energy'. Therefore, this remote resonance is also often used by alchemists as a time travel technique. We invite you to use this document as a real-time anchor to move your consciousness freely.

Energy and its handling. Yes, the first matter, the source material of all matter, is related to energy. But it is, to use a scientific term, 'potential energy'. So it is 'prima materia' because it cannot be used as it is.

Here, I want you to try to remotely resonate with the consciousness of Atlantis. They used the energy 'vril' very well. I want you to get a good sense of how they used vril. I think they collected it and they transformed it. They collected vril and alchemised it according to its use.

Now I want you to add Kabbalah as mental data to this distant resonance.

Specifically, the structure of the Sephiroth, the Tree of Life. The Sephiroth act as a framework of consciousness and as a prototype or template for holograms. This model has been used as a framework in consulting and application development. In today's AI, it is a blueprint for linking multiple models together as a system and creating differences in the patterns of their data processing.

Incidentally, the Sephiroth's Sephira is positioned as an AI model. However, it was not used as a statistical probabilistic logic, but rather as an organic artificial intelligence. It could be described as 'artificial bio-intelligence' in the modern sense, which also involved the use of vril. And of course it also interacted with mind projected holograms, genetic information, etc.

Try to go a little deeper and resonate with the technology. Energy is also related to the left and right pillars in the Kabbalah and how they interact. You must have felt this deeply. It is the pillars that the Hermetists call the 'gender principle', similar to the relationship between Kamimusuhi and Takamimusuhi in Japan. In the advanced stages of energy transformation, it could be compared to 'Izanagi and Izanami'.

From today, we want to abandon this old concept of gender. Of course there is the chaos created by the spiritual industry, but it is even more difficult to understand and out of step with the times. We are magicians and alchemists. So, as people who deal with technology, we want to redefine it as 'electricity and magnetism'. In this way we will understand that it is not just magnetic energy alone, but electromagnetism, and we will be able to construct holograms by interacting not only with electricity, but also with magnetic fields.

So when we go back in time and study energy from different aspects, we find out something important. It is the identity of the first matter. Here, I would like to express my personal radical view.

What is the first matter, the prima materia?

It is more than one. Strictly speaking, it is like defining something from several aspects, with one cause at its core. So the content that is projected depends on the purpose of the practitioner. For example, if you want to create something from scratch that does not exist in this world, or if you want to modify existing energy, the definition is different. But both are based on exactly the same material.

If we were to define it, what exactly would it be?

It is consciousness and energy. And energy is a material that is created by consciousness. So the alchemist who takes the first matter as pure consciousness is right, and the magician who collects unconditioned pure energy as prima materia is also right. This is how I understand it.

Therefore, all the definitions I mentioned at the beginning - water, chi, world spirit, ether and plasma - are also different aspects of prima materia. However, in a more precise definition, I see prima materia as consciousness itself, or pure energy.

And the energy-manipulating programmes we will explore later, the 'four basic elements', are also pure states of consciousness when we get down to it. So if we want to find pure, unconditioned energy as the first matter, we have to dismantle the four basic elements that maintain the status quo. This is what we call 'dismantling magic formulas'.
