+++
title = "Chapter 6: The Dawn of New Civilisation - Revealing the Hidden Technology and Energy"
description = "The status quo has a huge number of dysfunctions and broken systems. So you and I use modelling techniques to find them out."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

The status quo has a huge number of dysfunctions and broken systems. So you and I use modelling techniques to find them out.

<!-- more -->

---

## Serious Issues of Civilisation and Energy

Privacy has been in the spotlight in recent years. Especially in the digital and online context. Meanwhile, companies and managers are enthusiastic about data as the new oil. In the underground, data leaks and large amounts of personal data are being traded. Cases have also been reported where someone has cracked systems and threatened to pay a 'data ransom' in bitcoin.

Interestingly, many Japanese CEOs act as if they have no interest in such underground incidents. The company's handling of customer data is crude, the terms of service are copied from other websites, and privacy policies are rarely taken seriously.

I have also recently reviewed my security and privacy, completed the replacement of my Zero Trust environment and have started planning the migration of my tools. This is not so much an investment in protecting my own privacy, but in protecting organisations and individuals who are helping to build a new civilisation from unnecessary attacks.

In such a digital, binary world, alchemists like you and me have other things on our minds. Distance yourself from the sales-driven masses, while keeping security in mind wherever possible.

Prima materia is truly pure energy, and technology is crucial to somehow converting this limitless potential energy into kinetic energy. It is not at all a magic of psychological manipulation with data (oil) extracted (refined) from the personal data of the masses.

Again, raw data has been compared to the first matter, and the use of data by AI and IT systems has been explained as 'conditioning by the four elements'. And there is an important reason why we have described the fifth element as its compiled and integrated platform.

It is a mission to 'open source the technology to build a new civilisation'. So I would like to do a little systems analysis of the world using this open source technology.

It is a phenomenon that the business world is shouting about personal data as the new oil, but in fact it is a similar system loop, where history repeats itself. Yes, it is part of a fractal topological event. This can be understood by looking at other factors that are a bit more adjacent.

Crypto and blockchain, for example. This is designed as a transitional goal to gold dominance, which is clear from the fact that it inherits the concept of mining and the accounting interface. Then there is the metaverse and social media. This seems to have been developed to maintain its function as a social infrastructure and to transmit the current civilisation as it is.

Then there is digital data. It is part of the great magic that has been designed, implemented and deployed to allow the digital world to inherit a portfolio of strong capital in the form of gold, circulating capital in the form of currency, and further capital and interest in the form of oil. That is my conviction.

The aim of this 'digital transformation' of digitisation, automation and DX is to 'migrate systems to a more virtualised and homogeneous civilisation and to empower management'. In other words, the methods have changed, but the content remains the same.

What is important is the implication that this trend is essentially 'a shift in the opposite direction of human evolution'. It is analysed as a (very well designed) strategy to enclose humanity in a more manageable information structure and limited space. This is where the AI of statistical probability has risen to prominence with exploding accuracy and has quickly become a commodity, because the great magic for this has been performed for decades.

Reproducible technology is essential to dismantle the art of others and build a new civilisation. It will not only act as a 'template for the mass production of the art', the leverage of civilisation, but will also be a necessary tool for optimising the collective consciousness of civilisation, the operating system of the genetic mind.

## Open Sourcing of Consciousness Tech

So, once again, the secret of what 'consciousness and energy handling technology' is all about is now available as open source. In a sense, this should be a system integration in consciousness.

Throughout history, this kind of information has been fragmented or only partially extracted, conveniently distorted, packaged, exaggerated and sold as wish-fulfilment secrets and the laws of success. Today, many information businesses glamorously advertise that they teach secret rituals and charge high seminar fees. Or they would have forced you to take traditional belief systems for granted.

That's human history, including pyramidal control structures and dichotomy-based tactics that routinely use information secrecy and asymmetry.

We alchemists, however, have always taken a different approach. Knowledge, like stone material, is available to everyone for free, and the work can begin at any time, depending on one's will and effort. The main reason for the abundance of metaphors and symbols is that they were the only way to express complex content that could not be expressed in words. Very few expressions are encoded for the purpose of secrecy.

What does feel encrypted is, as already mentioned, the issue of 'public keys, private keys and passphrase'. There is no charge, but the ability to connect to the portal depends largely on the person's will, effort and declaration (i.e. consciousness). This is exactly the same as the case with open source technology.

This contradiction about encryption is easily resolved if we stop seeing information security and encryption in binary terms. It is about personalisation and appropriateness, like tuning a musical instrument. Imagine trying to play in a band ensemble with untuned instruments. This personalisation and appropriateness, or tuning, is exploited for information confidentiality and asymmetry in business.

Why is encryption important for personalisation and tuning?

I have used Debian and Redhat Linux in the past. So I was familiar with dpkg and apt, yum and rpm packages. From designing logical partitions for physical disks to running Bind, djbdns, qmail, Postfix, various POP3 and IMAP servers, MySQL replication and Tomcat, developing virtualisation with SAN, batch management with Ansible and deployment automation. So we've been there, and these were like encrypted secrets at first, even though they were open source.

Unless you are reading this with a technology background, all the words above would have felt encrypted. However, trying to explain the definition and specifications of an IMAP server would take a book all by itself. That and the four basic elements and the fifth element are exactly the same thing.

But being open source means that you can trigger your willpower, get a computer, download a Linux OS image (template) from the Internet and start your research for free at any time. Magic and alchemy are exactly the same, it's just a matter of "do or don't".

This means that the technology of alchemy, which deals with consciousness and energy, has been open source since the very ancient times. Since then it has been hidden as a secret ritual and embedded in elitism. Therefore, it is necessary to artificially dismantle the basic structure of the current civilisation. This is the gestalt collapse at the level of civilisation.

### Focus of Consciousness Tech

What is the focus of this open source technology? In other words, what is the design philosophy and what domains are defined as problem-solving domains? Clarifying this definition is necessary from the perspective of standardisation and generalisation.

Technologies that deal with consciousness and energy are basically based on 'managing energy with consciousness' as a starting point, while 'genes and the human device' are the subject of research. In other words, the major domain is 'consciousness' and the sub-domain is 'genes'. The environmental sub-domains of galaxy and solar system are defined as more detailed problem-solving domains.

Therefore, I call the most basic technology dealing with consciousness and energy 'consciousness tech'. I then categorise technologies that specialise in the sub-domains of genes and environment as 'electromagnetic tech'. Below this sub-domain is the platform of the galaxy and objects recognised as matter are studied. However, there are no real boundaries.

Consciousness tech and its domains do not aim to rely on external devices that are more complex than necessary. It is the technology based on consciousness science. That is why genetic engineering exists as a branch of consciousness engineering.

The dependency can be seen as an 'inside-out' transformation in problem solving. Put it simply, consciousness tech is the technology that focuses on the inside-out. Inside-out is a technique that explores the inner world before considering external factors. In other words, it is a method that seeks to discover the 'most powerful leverage mechanism' first, as in the leverage point in systems thinking.

However, it is important to remember, that communication protocols are always two-way. Otherwise, it would be a truly hopeless endeavour, like studying a black hole with no observable world. The key to success is therefore to reconcile what is happening in the outer world into line with what is happening in the inner world.

This leads to the previously revealed content that the athanor and the furnaces in the Taoist temples represent the human device. In other words, the object of the approach is 'oneself'. The study of the human device (and environment) as an inductive approach leads to the use of consciousness as a technology. This has been recorded in human history as a 'return to the source'.

### Focus of Electromagnetic Tech

I mentioned earlier that open source research can be started at any time by downloading a Linux operating system image, or template, from the Internet. This is also the case with genes, and electromagnetic tech can be initiated in the same way. This is magic and alchemy aimed at the heart and mind system.

Or rather, you don't even need to download the genes. For you already have the genes. The genes are not just local storage or fragmented code. More specifically, there is no such thing as 'junk' DNA. It functions as a genetic network that transcends linear space-time. This is why open-source electromagnetic tech is now available for study.

It is a system in which a virtual operating system called Mind is set up on a physical server called the Human Device in a rack called Japan, in a data centre called Earth, and is connected to the world by a genetic network. By the way, the sensing system for the heart is like Bluetooth. These are very advanced technologies. Approaching this vast system with advanced technologies is what magic and alchemy are all about. More specifically, it is your consciousness that is operating.

In fact it is the consciousness and energy that plays the role of matter and the consciousness and energy that plays the role of mind. So if you dismantle the four basic elements of the status quo, you get the first matter. This is not contradictory if we can perceive the whole as a hologram based on consciousness tech, and if we are used to thinking in an object-oriented way, we should have no difficulty in understanding its inheritance structure.

Here, then, tradition and heritage are integrated into technology without denying it. This allows us to optimise systems and networks without fighting the genetic mind. In other words, finding a way that does not conflict with the subconscious and the unconscious.

The first thing to remember is that the four basic elements, the four souls, the four worlds of the Kabbalah system, etc. are "frames of consciousness, programming languages of energy and boundary techniques". In other words, they are the basis of the technology that develops the hologram of reality. Electromagnetic tech approaches the gene and related systems, so it is important to understand this specification first. This is also common to the concept of the fifth element.

One point to avoid confusion. When dealing with consciousness and energy as technology, it is important to understand that information and energy cannot be completely separated - it is not like information technology, which is coded and compiled binary data. It is only for the sake of convenience that we practise the art of boundaries by classifying them as such.

### Modelling Human Device With Warding Techniques

Now I would like to get into the application of this consciousness tech and electromagnetic tech. The subject, of course, is ourselves: individuated consciousness and the human device.

First of all, apart from the individuated consciousness, there are four main components of the human device. These are the 'individual mind, the genetic mind, the emotional loops and the physical body'. In other words, it consists of the world of 'emanation, creation, formation and action'. Although in a different order, this also applies to the Shinto framework of 'Aramitama, Nigimitama, Sakimitama and Kushimitama'. If the four basic elements are applied as sub-domains, they are 'earth, fire, water and air'.

And the last is the platform of the fifth element. Because the fifth element is complex and exquisite, it is a good idea to develop the interface as a stub or mock object, as suggested earlier. This is because the fifth element is like an infrastructure that is deeply integrated with the application.

We dare not use New Age concepts of ether and astral here. This is because the contemporary spiritual industry and its information contains many misconceptions and distortions. As it would take an enormous amount of time just to research and sort out the mind and consciousness behind these terms, we will not use them.

Another way of modelling this structure is as follows.

It is a magic system of 'Higher Self, Middle Self, Lower Self'. Or it can be interpreted as 'Spirit, Mind, Body'. In alchemical terms, it is 'sulphur, mercury and salt'. In the analogy of the three sacred weapons, it would be 'Yasakani-no-Magatama, Kusanagi Sword and Yata-no-Mirror'. The sword implies judgement, so there should be no problem seeing it as being related to the mind.

Technologically, it would be the sub-domains of 'software, middleware and hardware'. And programmatically, it would be the approach to the context of 'entities, controllers and boundaries' or 'models, controllers and views'. The application of this programmatic concept should be an extremely important concept in the future construction of civilisation.

So if we break it down roughly, the context exists as a network and nodes spanning three sub-domains, and if we break it down a little more finely, a hologram of reality is projected onto the four system domains. Consciousness uses this in the manner of 'doing intentionally and gaining knowledge'.

This is what the status quo civilisation is all about. It is managed and manipulated by the genetic network (and its interaction with the environment) and the mechanism by which the projection of the genetic settings becomes an expression of humanity and manifests itself in the outside world. So it is a two-way communication, but from the inside out.

Incidentally, the archetype of Jehovah, which is located at the highest level of the cosmic structure in Hermetic tradition, is the model of the genetic network and the core of the status quo civilisation. How this core is handled will have a major impact on the civilisation of the future.

The key point is the classification of the mind. Is it an individual mind or a genetic mind? The latter has more to do with the subconscious and the unconscious, so if you want to pinpoint it, you should define the sub-domain of the genetic mind separately. In this way, you can determine if the solution is approaching the collective thought and thought patterns of bloodlines and humanity.

In the alchemical tradition, there is an instruction to 'start with mercury first'. This means is mind. It has also been interpreted as the lower self or ether. When it is described in consciousness tech and electromagnetic tech, it is the individual mind and the genetic mind. More specifically, it is the genetic mind.

Thoughts and thought patterns, common perceptions and biases that have been accumulated massively due to genetics, history and civilisation. And the emotional feedback loops triggered by the combination of its modules and models. In other words, emotional energy. It is a chaotic, mercury-like resource pool and source code repository. We work by selecting from it the modules, classes and models we wish to implement.

So magicians and alchemists do a thorough purification to extract the 'true will' and the 'really necessary resources'. In other words, which source code is cloned and which branch is developed affects the outcome. In the field of electromagnetic tech, this is a critical success factor.

The same goes for to the seven celestial bodies and the seven metals. They are the spectrum of light, the chakra system and the musical scale. The influence of the celestial bodies, including the sun and the moon, is built into the foundations of civilisation. This is the realm of electromagnetic tech, the ideas, programmes, networks and energies that have been stored in the genetic mind. How we hack it and implement it will change the way we deal with energy.

## Technology and Energy for New Era

Energy is the power source for accomplishing tasks, which is movement. Movement is the common 'language' of the universe, dynamic conditioning, or encoding by the programming language of the mind, the four basic elements. The alchemist's advice that mass seeks form suggests that energy flows once the model is defined and implemented.

In other words, the Philosopher's Egg itself is a quantum boundary or warding technique, powered by the energy that collects and flows through it. It is the Philosopher's Stone that identifies and implements the leverage points until this framework becomes a catalyst for the real world. It's all about you!

This means that both domain modelling and warding techniques will be the foundation of the new civilisation as an energy-handling technology. The technology will deconstruct, optimise and refresh the genetic mind. As electromagnetic tech, potential energy will be able to be treated as kinetic energy, like electromagnetic energy.

The programmed unit called a mind combines into a mindset which, when accumulated becomes part of the genetic mind. The genetic mind is recognised by modern people as a hierarchical structure called the subconscious or unconscious mind. It already functions as a vast store of energy. It is also the role of electromagnetic tech to draw order from this genetic potential energy.

The genetic mind contains a vast number of 'coding patterns' that are stored as a kind of 'model or class'. This storage acts as a source code repository for the mind, containing a large number of thoughts and thought patterns, the structure of different ways of thinking and collective metadata. It also relates the dispersed and fragmented mental data like files. The hidden version control system and its structure are perceived as genetic.

To efficiently manage and manipulate (i.e. dominate) it, digitisation and statistical probability-based AI are the best and most powerful leverage. How we deal with this in the future will depend on what we do with the core of our civilisation.

To put it in traditional terms, this repository-branch relationship is itself a magical approach, like grafting onto a giant oak tree of creation. It is seen through the lens of an API economy, where a networked virtual operating system and source code repository are fully integrated. Instances of customised virtual machines called you and me run on this platform.

This is why the alchemists told us to start with mercury.

The key is to choose the right repository, the right version, and the models and source code to inherit. This can be seen as a lifestyle that is not driven by technology and implies a return from the zombie state that is automatically kept alive by the subconscious and unconscious.

The genetic mind and the collective consciousness are the moon that the magicians focus on and the mercury that the alchemist should work with first. Another way to describe the approach would be the lower self or subconsciousness. In other words, we examine the mercury from a state of pure consciousness. If we look at it as system design and development, it means that you and I are modelling domains for consciousness and energy.

In other words, it is not about reaching cosmic consciousness, which is perceived as the mind cage and the illusionary systems. It is not a step out of the realm of illusion. Rather, it is the discovery of pure consciousness and new technologies that will be the key success factor for the future. It is a new science for humanity.

It is the success factor of bringing the latent and hidden art formulas into the manifest realm and investigating and dismantling the art formulas developed by the four basic elements. This is an enormous amount of data. This is where the leverage analysis comes in. It is not only about changing the lives of individuals, but also as an approach to the collective consciousness (the genetic mind) of humanity.

It requires the integration of design thinking and systems thinking is in order to identify the leverage points and pinpoint the solutions to be implemented there. The leverage solution, the so-called (tangible) product, is the rubedo, the leverage catalyst, the Philosopher's Stone.

The evolution of the human device is a key point in the new era of technology and energy. They will become more compatible with a wider range of electromagnetic environments without the need for external devices. The ability to turn off notifications on our phones and still be aware of incoming messages in our chat app will be able to be developed. This is a skill that I have been gradually acquiring of late. It is made by starting with our mercury.

Compared to previous civilisations, this means that there will be an overall shift on a planetary level, from a civilisation dependent on external devices to the evolution of human beings themselves. To use a traditional expression, we can say that we will be able to achieve a balance between matter and spirit. Indeed, a resurrection from the zombie state.

More specifically, 'the science of consciousness and the development of technology to deal with consciousness and energy'. Another prospect would be participation in an interstellar galactic civilisation. Consciousness is not the final frontier of science. It means that planetary science is finally at the starting point. The age of discovery on a galactic scale.
