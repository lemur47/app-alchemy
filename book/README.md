# The Secret Portal of Alchemy

What if all the secret manuscripts of alchemy are unencrypted? Magic and alchemy are the science and technology that most people don't know about yet.

[>> Read online or download a PDF copy](https://www.ethical.works/book/)

To achieve sovereignty and freedom, you might want to understand how the status quo civilisation really works, and how jamming techniques and hidden formulas prevent you from doing it.

## Table of Contents
First of all, you must unleash your abilities to challenge the status quo - this is called Awakening. With your consciousness and mind technology, you will find your own way into the new era of complexity and transparency.

### [Preface: My Profession Is Alchemist](./preface.md)

To share the knowledge I have gained through many experiments, I would like to introduce myself and the current situation on Earth.

### [Chapter 1: Job Description of Alchemist](./chapter1.md)

Before we get into the details of magic and alchemy, you and I need to know what magic and alchemy really are.

### [Chapter 2: Discovery of Prima Materia, and the Public Key to Decipher](./chapter2.md)

To find out what the prima materia really is, you and I will travel beyond space-time using an ancient time-travelling technology.

### [Chapter 3: Breaking the Chains - Hack Parasitic Programmes, Dismantle Your Mind Implants](./chapter3.md)

To dismantle the great magic and its formulas, you and I will log into the secret portal of the alchemy network.

### [Chapter 4: Awakening the Fifth Element - Finding the Path to Human Evolution for the Future in 2030](./chapter4.md)

What is the 5th element? How can we make the most of it? To find out more about the 5th element, we take a technological approach.

### [Chapter 5: The Art of Consciousness Hacking - Shaping Reality with Technology](./chapter5.md)

There is more information than ever before, a technological approach can help you create order out of the chaos.

### [Chapter 6: The Dawn of New Civilisation - Revealing the Hidden Technology and Energy](./chapter6.md)

The status quo has a huge number of dysfunctions and broken systems. So you and I use modelling techniques to find them out.

### [Chapter 7: Taking Back Your Freedom and Sovereignty - Open Source and Innovation Strategies](./chapter7.md)

Virtualisation - Identifying how our will and freedom have been taken away through control and hidden manipulation.

### [Chapter 8: Dismantling the Hidden Magic Formulas - The Technology That Keeps You From Sovereignty](./chapter8.md)

Like bending reality, why have your magic and efforts failed so often? One of the root causes is a hidden technology and well-designed systems.

### [Chapter 9: Building Your Own Path - Using Both Consciousness Tech and Electromagnetic Tech](./chapter9.md)

With sovereignty and freedom, you collect energy by developing your own app and interacting with the environment as a catalyst.
