+++
title = "Chapter 1: Job Description of Alchemist"
description = "Before we get into the details of magic and alchemy, you and I need to know what magic and alchemy really are."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

Before we get into the details of magic and alchemy, you and I need to know what magic and alchemy really are.

<!-- more -->

---

## Prepare Yourself for Magic and Alchemy

What is magic in the first place? What exactly is alchemy?

Many people have imagined, dreamt and put many interpretations on it. So there is a lot of literature on magic and alchemy. It is actually very doubtful whether it is really necessary to study it all.

Modern magicians and alchemists can also use search engines and AI summaries to a certain extent. That does not necessarily mean that the literature and summarised information found in a search is necessarily correct.

One reference written by a Hermetist stated. Alchemical books are mixed with false information and there is a fair amount of misinterpreted literature. Therefore, they said, it is not necessary to research everything, but it is important to discover good quality information.

According to one hidden magic organisation with an organisational structure similar to an adventurers' guild, once you have found a few good models that suit you, you can implement them.

This is exactly the same as the Internet and books today. More specifically, it is similar to choosing a coach or consultant. In a sense, this is the dividing line between success and failure in our magic and alchemy.

There is no point in just dumping in a lot of knowledge and taking in all the right and wrong information. Furthermore, if the knowledge is not put into practice, it is almost meaningless. More to the point, if you don't develop yourself to discover good information, there is nothing but despair.

The same goes for yoga. Think about collecting all the information about kundalini awakening and disseminating it on social media. There is nothing wrong with that in itself, but if you don't practise it, it's pointless. If you don't actually experience an increase in energy and intense stimulation of the pineal gland, it's not very convincing.

"All right! I'm going to awaken my kundalini, become a guru, and earn fame through spiritual business! So just do some stretching, practicing a few rounds of deep nasal breathing, and try some 'the breath of fire' and hip-shaking". Wait, it doesn't work like that way.

## Small Start Strategy

At the beginning of my long, long alchemical journey, there was one thing that was fortunate for me.

It was then that I began to observe the world, meditating only on the writings of the Emerald Tablet. I actually travelled to seven countries, including Japan, to do my research. Just a few lines of information. I observed the natural world, thought about it every day, and tried it again and again.

Of course, the natural world includes the human world.

So even when I was doing marketing analysis, designing KGIs and KPIs, preparing proposal documents during consulting sessions, or presenting to clients, I never forgot about the Emerald Tablet. Not even in those moments when I was mixing coffee and MCT oil in a blender and exclaiming things like, "Prima Materia! Spiritual emulsion!"

But if I'm honest with myself, the hidden truth is that I didn't start strategically small at all, I just didn't know what to look for. All I had to do was remember the Emerald Tablet and I was off! It was a simple approach, driven by cheap motives.

But it is also true that this simple, thoughtless and cheap choice allowed me to proceed with a practical focus and not be swayed by superfluous information. For this, I would like to thank the dense genetic network of alchemists and secret guides that has been linked to me since ancient times.

I would also like to recommend this small-start strategy to those who are just starting out in magic and alchemy.

## Is Alchemy Similar to Agriculture?

Well, if you read old literature or look at illustrations, you will find that alchemy is described as if it were agriculture. Yes, alchemy is the agriculture of the celestial world. It really is otherworldly agriculture!

The soil is cultivated, the environment is prepared and the seeds are sown at the right time. Then, while watching for the same optimum timing, they silently nurture the crops and wait for the harvest. This must be done in the heavens, in the invisible world.

I know this sounds like an illegal cannabis farmer growing cannabis hydroponically in his closet.

Surely, if the farmers have changed jobs, they must have become powerful magicians and alchemists. They must have farmed in the past with a strong emphasis on belief in birthplace and interaction with earth spirits, so their genetic preference settings are perfect.

Basically, both magic and alchemy look at energy in the same way, in cycles of a year. The lunar cycle is important, and the timing is like the scheduled execution of a server program.

I now activate my magic formula for the full moon, based on the goals and plans I have set for the new moon. While activating magic as big as missions and visions in business, I do several magic at strategic and tactical levels in stages. Tactical level magic includes daily routines and morning rituals.

And today is a full moon. The full moon is similar to the summer solstice, represented in alchemy. From here, the success of the practice is successful can be determined by whether the harvest begins. Small magic at the tactical level begins to harvest relatively quickly, but at the strategic level, the level of difficulty increases dramatically.

In other words, it is not a good idea to just throw everything into the mix at the drop of a hat.

This is where training that combines systems thinking and design thinking is very useful. Multiple mental models and mindsets are the most powerful leverage because they require focus and observation.

However, what surprises even me, who is rated as 'non-operational and not good at completing tasks' in the aptitude assessment, is the lack of focus of people today.

Whenever I see them glued to their smartphones at all hours, they seem to have an unusual level of concentration, but they are actually quite different. They are constantly drifting through thin information, wandering daily through a maze of scraps in search of instant gratification and instant answers.

In such a state, the success rate of magic and alchemy is extremely low. Rather, it is the result of being addicted to someone else's techniques of manipulating primitive concepts, and is tantamount to being unwittingly subjected to a cancerous magical assault (i.e. a curse). Indeed, mind control is a form of magic.

## That Magic That Has a Wide-Ranging Effect

In fact, the qualities and elements that increase the success rate of magic and alchemy are no different from those in business and real life.

This means that short periods of deep concentration are essential. And that it must be maintained over a long period of time. It also means that a wide range of deep information must be sought. And that is what causes the barriers to entry to explode - no one can tell you the answer.

When you put all these things together, you realise that what you need for magic and alchemy is an acquired predisposition and analogical thinking. It helps in formulating business strategies, developing tactics and the devising of operational techniques.

In celestial agriculture, the big picture, the order and the tasks are very important. It is the same in business, where there is a big picture, including mission and vision, which is strategically translated into a non-linear process, and then the tactics that reflect that process are implemented as tasks. Furthermore, it is not a fixed plan, but requires flexibility to be able to fine-tune it in a flexible and agile way in response to changing times, situations and environments.

In modern terms, magic and alchemy are easier to practice when IT developers who have done systems analysis, database design, domain modelling and optimal context segmentation, can formulate strategies like consultants, while prototyping with design thinking.

To make it easier to understand, it is the same as a developer finding a problem by himself, designing a smartphone app, coding it by himself and using it by himself first. Then it goes to the market and everyone uses it together. Magic and alchemy are very similar.

This is because, like systems engineering, magic and alchemy are the art of constructing and implementing systems in the invisible world, using visible objects as 'conceptual devices'.

In other words, if you start on the path of magic and alchemy, and then continue to practice it, it will work perfectly in other areas. If you are a manager, it will help you with new projects, and if you want to live a healthy life, it will definitely improve step by step.

If you think about it, you can see that just as the etheric body is present in everything, systems in the invisible world can be used (deployed and installed) as software in the visible world. There is a big difference in practice between knowing this mechanism and not knowing this mechanism.

## The Truth of Magic and Alchemy

Let's go back to the first question. What is magic? What exactly is alchemy?

Is it difficult to express it in a few words? No, it is not difficult at all. Magic is the science of the invisible world and alchemy is the technology of consciousness and energy.

That's why it works in so many different areas at once, including business and life. And analogical thinking is effective because consciousness and energy are analogous. In other words, it is fractal and topological, so tele-resonance can be used. So it is a technique that also works in the electromagnetic realm, with different technologies available for each stage of materialisation.

But the essence is very simple: it is all about consciousness and energy. This is exactly the same for magic and alchemy. Therefore, we will now look at 'materials and the programmes that deal with materials'.

This is what the ancient alchemists called them. Prima Materia and the four basic elements.