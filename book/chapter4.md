+++
title = "Chapter 4: Awakening the Fifth Element - Finding the Path to Human Evolution for the Future in 2030"
description = "What is the 5th element? How can we make the most of it? To find out more about the 5th element, we take a technological approach."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

What is the 5th element? How can we make the most of it? To find out more about the 5th element, we take a technological approach.

<!-- more -->

---

## The Ideal Path for Those Who Struggle in Life

Magicians and alchemists. An important profession of super-ancient times and people who have been persecuted in modern civilisation. A path that will play an important role in the new civilisation and its building stages, which will manifest in stages in the near future. A specialised field that will be increasingly activated over the next few years.

The tips, qualities and work content of these people have already been mentioned. However, I would like to mention again those who have an aptitude for it. There is an aptitude for every job. This is my personal opinion, but I think that people with the following characteristics are suited to magic and alchemy.

It is gifted people who struggle to fit into society. People who are 'ambidextrous' rather than left-handed, whether inborn or acquired. Vegans and vegetarians who respect the environment and life, whatever their initial motivation. Mechanics who are good at identifying the essence of things and analysing systems. Creative people who develop ideas and communication content as three-dimensional information structures (i.e. holograms) and solve problems from a different perspective to others, but who have been taken advantage of and ignored.

More specifically, they are people who are good at association games, who do not believe in real stories and myths, and even take anime and fantasy as objects of study, and can use analogical thinking to read deeply into them. And people who realise that the 'mechanisms' of magic and alchemy are very systematic. People who can see things in a non-linear, object-oriented way. Finally, and most importantly, people with a strong desire to make full use of these wisdoms and technologies to change the world into a better place.

The common denominator is the difficulty of living. That they are small voices that have been crushed in a majority society. That they have always been forced into unwanted roles in homogeneous and uniform cultural practices. And that they have a peculiar perception that the masses see as delusional or psychotic. They rarely stand out on social media and are either invisible or the target of attacks in the office.

In fact, as a result of these negative and weak points in society that need to be overcome, people who were previously excluded will find themselves in the 'impactful careers' needed for the civilisation start-up that is about to begin. This is because the changing and growing planetary energy means at the same time 'increased information density and cognitive load', which is a harsh environment for the dominant inclinations. They will probably have their hands full adapting to their own environment, making it extremely difficult for them to handle large amounts of information and energy instantaneously as part of active, highly decentralised and autonomous organisations.

This stage has already begun. It started when: around 2020. The period from now until 2030 will be the 'environmental adaptation phase' for the masses and the 'civilisation building phase' for the few. This is not a question of superiority or inferiority, but a stage handled by those who are good at starting from scratch, with the inclination described so far, meaning that it will be an era of cultivating the civilisation. In the stages that follow, groups of people who are good at expanding and improving will be activated and will come onto the stage.

Activation is what will become important here.

This activation begins with the dismantling of the four basic elements and the discovery of the first ingredient in alchemy. In modern terms, this can be described as a complete detachment. It is therefore suitable for vegans and vegetarians. The same goes for ambidexterity. In other words, they are the kind of people who dismantle the stereotypes and the peculiarities of civilisation, stop dancing on other people's magic platforms and start all over again from the essence, with a complete reset.

The most obvious process of activation is what psychology calls 'shadow work'. This often refers to the integration of consciousness and emotions that the person has ignored or sealed off. In technological terms, it means releasing conditioned minds and energy, whether in the dark or in the light.

To add to the clarity for server administrators, it is the same as what happens when log files overwhelm disk space or logical partitions and processes cannot function. It is no different from deleting unwanted logs or compressing them and moving them to a tape device or a high-capacity hard disk. Whether it is error logs or access logs, it should not discriminate. Think about this: would you denounce error logs as a conspiracy of darkness and keep access logs as a force of light?

Therefore, to avoid bias as much as possible, it is recommended to regard it as a way of freeing up memory space on your laptop. This is because restarting from a full reset means a collective surgical dismantling of light and dark, positive and negative, likes and dislikes. Remember how refreshing it was when you reset your phone to factory conditions? By the way, I would like you to think of the overhead (hassle and cost) of updating the operating system again as an investment in transforming the lead into the pure gold.

Thus, dropping the old energy and programmes while thoroughly dismantling them will not only give you the first matter, but also help you to understand the programme specifications of the four basic elements. And beyond that is the core element in reality creation, known as the 'fifth element'. That core element is also an essential material for inherited magic and alchemy. And the inter-dimensional portals for entering the realms that deal with that material are also discovered in the process of intensive detachment.

The technology of super-ancient civilisations working with consciousness and energy, the magicians and alchemists in charge of it, the knowledge of detailed system specifications, plus the secret alchemist network, sometimes requiring shadow work in the subconscious dungeon, which is like a fighting dragons, the fifth element beyond it and the inter-dimensional portal. When all of these are in place, humanity can take its evolutionary path in its own hands, from the individual level to planetary civilisation.

There is therefore no saviour, no one to take the place of this 'magnum opus' for us. If I had to define it, I would say that it is "the 2-3% of the world's population that has been rejected so far". Interestingly, it also coincides with the innovation adoption curve.

## The Truth of the Fifth Element, the Challenges of Civilisation

The fifth element, Quintessence. Do you already know what it is?

If you are too deeply absorbed in the spiritual industry, you will try to define everything as ether, but that is not good enough. It would be the same as distorting information about high vibrations and frequencies as 'ether' on its own. We want to dismantle those powerful biases right here and right now and release the energy.

On that premise, let's again ask the search engine-linked AI to summarise what the fifth element is in magic and alchemy. Because when you remove the bias and then look objectively at the bias, you will discover something new.

> In both magic and alchemy, the fifth element is often referred to as aether, or "spirit". The classical elements of earth, water, air, and fire were believed to be the basic elements of all matter in the physical world, but aether or "spirit" represented a higher, ethereal realm beyond the physical. Aether was sometimes associated with the universe as a whole, or with the concept of the divine. While not universally accepted, the idea of a fifth element, whether it be aether or other substances, was prevalent in many ancient cultures. The concept of the five elements has continued to have a presence in modern Western esotericism and some contemporary magical practices. However, it's important to note that these concepts are not scientifically supported and are considered only within the realm of belief, rather than empirical fact.

Don't make the mistake of shouting here, "The fifth element is plasma!" Rather, it's a starting point for reframing this summary as a common perception throughout human history, a stereotype and an object of decomposition . It requires critical thinking and multiple options for consideration: is the definition really correct, is the problem appropriately framed, are there other, more alternative interpretations?

Considering the historical background, there is a sense in which the concept of a fifth element was added to the theory in order to make it viable. There is also an impression that it is like an add-on to make it possible to explain phenomena that cannot be explained by the four basic elements alone. This is not a particular problem, as similar things are observed and verified in today's science. However, it is extremely important to question the problem and assumptions before taking a hypothesis on board.

By their very nature, magic and alchemy have been systematised in a way that follows existing frameworks such as Greek philosophy, Sumerian mythology, Babylonian astrology, Christian Kabbalah and the terminology of metalworking. One reason seems to be that description in a common language was optimal in every sense. Another is a hidden secret, but I am convinced that the ideas were deliberately passed on in order to re-use the already developed genetic mind, programmes and energies of the collective consciousness.

Here is a summary of the terms associated with the fifth element. They are: ether, spirit, anima mundi, which is the world spirit, and prima materia, which is the first matter. The word spirit can then be translated as soul, referred to as spirits, or categorised as mentality. In bookshops, books on spirituality are categorised as 'spiritual world'.

In other words, something of an elusive and unfamiliar nature is considered the 'fifth element'. This is similar to the definitions of mind, mental, spirit, etc. So, it has not been proven by science on this planet and centuries have passed with all kinds of hypotheses and speculations.

However, this hypothetical element of an elusive and unfamiliar nature cannot be left untouched. It's like solving a problem: you can't solve things that are too vague. And the technology we are focusing on cannot be treated as reproducible.

But, there are ways. Technologically, we could adopt the approach of modern science and treat the fifth element like a stub or a mock object. Yes, treat the fifth element like gravitons. From that point on, however, a thorough deconstruction and large-scale dismantling of the art will be necessary.

This is because throughout history, this planetary civilisation has built its belief systems on dichotomies such as 'matter vs. spirit', 'body vs. mind' and 'astral vs. ether'. This is evident in the strong inheritance of such oppositional structures as 'real world vs. spiritual' and 'material vs. spirit' in New Age thought and the spiritual industry.

In fact, it must be said that to build a civilisation on this idea is to build things on a foundation that is as fragile as building a civilisation on absolute sound. This is because it is not designed to change according to relativity, scope, environmental variables, etc. It is not a dynamic civilisation, but a fixed bandwidth. It is therefore extremely difficult to increase information density as it is.

I think we have once again realised that what we have to dismantle is the world itself as we perceive it. So it is a powerful method of dismantling that will overturn the very foundations of planetary civilisation. To achieve this, we must have the courage to collapse the gestalt of tradition, folklore, common sense and belief systems.

## Redefining the Fifth Element, Evolving Technologies

So, let's think again. What exactly is the fifth element?

Personally, it is perceived as a kind of platform with multiple properties at once. It plays an integrating role in making things work as a system, but also acts as a communication protocol, provides an interface to any object or instance, and also includes logic and models. It is such an idea of the common compiled binary platform of the universe that it is no longer possible to completely separate it as a function or role, such as matter or life force.

Whether this can be used as an argument or not, from my personal experience is that neither the etheric body nor the etheric world of things exist completely separate from each other. They literally seem to merge with the environment, and the etheric body in my own body seems to merge with space. Even in the area perceived as the astral world, form maintenance and separation are impossible, and one feels a dragging sensation and a dynamic silhouette.

It is therefore perceived as being an integrated platform, but at one with the app and the user. This is also the case with the relationship between the four basic elements and the fifth element, which cannot be completely separated from each other. The same applies to sulphur, mercury and salt and the three sacred elements, which should not be seen as separation but as an integrated property of things (a fusion of attributes and properties in objects).

This means that the functions and properties cannot be completely separated as hardware, middleware and software. In other words, the division into Higher Self, Middle Self and Lower Self is virtually impossible. For the sake of simplicity, magic and alchemy only implement the target of the approach as a problem-solving domain, i.e. domain modelling, and the context for the purpose as a formula.

Without an indivisible platform, the communication and technology that you and I are using right now, such as 'alchemy of forms' or 'philosopher's eggs', would be unusable. Genetic engineering that allows cellular level holograms and their collective individuals to interact with the planetary environment would also be impossible.

Shinto interpretations of spirit seem to best describe some of the properties of the fifth element. This is because in Shinto defines spirit as a 'knot/musuhi'. This is the hidden vessel, or compiled platform, that allows the dispersed things to function as a system. It is also corresponds to the concept of alchemy.

We do not want to define it crudely as just cosmic energy. So what does the fifth element integrate, what does it combine, what does it bind?

It is objects, instances, modules, classes, methods, interfaces, etc. In other words, it is the energy and its conditions that are conditioned by the four basic elements and have certain properties. In mystical terms, the fifth element is the application or platform of the programme of the mind, the hologram of reality, the distributed mental database, the psychic controller, and so on.

In other words, part of the fifth element functions as an etheric body, which also functions as a hologram of mindset. It serves to connect scattered and fragmented mental data and generate a hologram in the name of reality. The same applies to the electromagnetic fields around the physical body. They are operated and maintained by energy conditioning and the consciousness that designs and experiences them.

So every energy body is not hierarchical. It is the same with dimensions. For convenience, it is simplified and explained in terms of hierarchy for ease of understanding, but it is just a map, not the land itself. So there is no spiritual ranking of 55 dimensions as a common perception of the universe. To avoid reinforcing such illusions, we technologists experience the land based on maps, emphasising both design and implementation, and actively integrating history and experience.

And we should rethink the whole universe, going back to the most primitive level of consciousness and energy.

So how can we use this redefined fifth element or four basic elements to create civilisation and reality? There are two main ways. Ideally, we should not use one or the other, but both. I will now explain the methods and share abstract strategies at the same time.

One is to go back to consciousness and energy and develop from scratch. I call this 'consciousness tech'. In this method of development, it is necessary to forget about the fifth and the four basic elements, to thoroughly discard the existing framework and to dismantle a large number of formulas. This is what is known in alchemy as the 'transmutation of the philosopher's stone'. However, it should be understood separately from body transformation, such as ancient Egyptian sexual magic and Chinese inner alchemy. Of course, they cannot be completely separated.

The other is extended development, which uses existing frameworks and belief systems. It is easier to understand this as the development and operations (DevOps) phase of extending a minimum viable product (MVP) in a startup. I have called this development approach and technology 'electromagnetic tech'. This is because it uses existing electromagnetic fields and associated systems, which act as leverage points across time and space.

The approach of utilising electromagnetic tech is favoured by many magicians. However, this approach is only effective when tradition and the purpose of magic are in harmony. Activating a formula by observing the cycles of the year, the cycles of the moon, etc., is also fundamentally in the realm of electromagnetic tech. It uses the 'genetic mind' of tradition as a framework to leverage and automate the implementation.

However, there are exceptions. This is because the inconsistencies in the system can be resolved if the practitioner is at a level where they can hack and modify the collective consciousness and genetic mind. However, as you can imagine, the consequences of failure are also very high and often very risky, so it is not recommended.

However, the success factor in building a civilisation is to use both consciousness tech and electromagnetic tech for different purposes. For example, if you want to build a new civilisation by completely removing genetic minds and technologies that are related to manipulation structures and brainwashing, you definitely do not want to inherit existing frameworks. The strategy then becomes 'to develop a base hologram for the transition from scratch'. Conversely, if you want to make full use of purified bodies and activated genes as devices, you should adopt electromagnetic tech.

In any case, whatever modelling and design you use, the basis is 'consciousness and energy'. And the principle is 'fractal and topology'. I have visualised the process of observation and thought so that you can experience how an alchemist perceives consciousness and energy as fractal and topological. If you are the type of person who likes to understand visually, you will find this useful.
