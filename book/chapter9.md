+++
title = "Chapter 9: Building Your Own Path - Using Both Consciousness Tech and Electromagnetic Tech"
description = "With sovereignty and freedom, you collect energy by developing your own app and interacting with the environment as a catalyst."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

With sovereignty and freedom, you collect energy by developing your own app and interacting with the environment as a catalyst.

<!-- more -->

---

## Find the Formulas That Hold You Back in the Latest Trends

There is a new trend called 'Big Tech away'. I'm actually one of them.

Of course, it's hard to get away from Big Tech completely, and I'm not against Big Tech. It has brought considerable convenience to the world. But it is also true that it is virtualised and ridiculously deeply embedded in systems designed to 'control and manipulate'.

Take the example of one of the world's biggest online retailers. If you use its video streaming service in a privacy-conscious environment, you will find that more than 90 trackers and external services are detected and blocked.

In other words, while you are paying an annual or monthly subscription fee, you are being subjected to incredibly detailed data collection, behavioural history tracking and analysis, and routing (manipulation and redirection) in the name of artificial intelligence 'information filtering'. As long as the competitive principle of the artificially created market is the rule of the system (i.e. the business rule), this 'centralised resource management' of human ATMs and related data is intentional.

And since the market is a sub-domain of the economic platform and political and governance structures, it still inherits a funding and management philosophy aimed at 'control and manipulation'. This is actually exactly the same as the spiritual industry being a sub-domain of 'mind cages and illusion systems'. Even in the religious industry, where there may be a black box organisation called a governing body, we find that they all inherit the same architecture and system.

This is why I am moving to apps and tools with a mission of security, privacy and free will. With the exception of physical devices, 90% of the tools have been migrated. This is the 'athanor' of the modern alchemists, and this kind of great work (great magic) with conceptual devices that 'equalise the inside with the outside' is the foundations of civilisation.

As the purification process of Big Tech disconnection continues, all sorts of problems arise. For example, just trying to cancel a virtual workspace creates an extraordinary number of obstacles. Firstly, the cancellation process is a maze. All the systems are intertwined and cannot be cancelled. You cannot contact support. You can't solve the problem by reading the documentation, and other techniques that hold you back are activated.

The same is true of the services provided by the companies that run the country's biggest shopping mall and financial services, where it is impossible to contact support and only a great deal of time and energy is wasted. This world is dominated by systems that provide only one-way communication for the convenience of the provider. Social media communication is also a one-way street, and the world has become a place where human conversation is no longer possible because it is equal to the structure of the system.

This technology-based jamming technique is commonly referred to as the 'dark pattern'. And Big Tech makes extensive use of intense dark patterns. And on the infrastructure provided by this Big Tech, global tools and apps are made available that affect all services. This is the multiple inheritance of art.

When this happens, the virtual workspace cannot be cancelled and the account remains, making it impossible to log in to other services via Big Tech. For well-designed and implemented tools, for example, a password reset (unspelling) can solve this problem if the email address has not changed, but for companies committed to speed and low cost, even this is not possible. In some cases, it can take several weeks to get an initial response.

This is a simple example of the technology-leveraged formula that is holding you back. You will notice that this is not just a metaphor or an analogy. The issue of technology itself is directly related to the jamming technique and to the civilisation of domination and manipulation. And it has been carefully designed, developed and expanded primarily through electromagnetic tech. The design philosophy is exactly the same: you need US dollars to buy gold and financial data is collected in NYC.

In human terms, the strategy is 'incremental, non-linear and dynamic positioning', which means reducing information density and achieving vision and mission through individualised and decentralised movement. In electromagnetic engineering terms, it is 'operating a massive volume of electromagnetic feedback loops'. That is, it is firewalls and routing, and it is the heart- and mind-focused holograms that manage the structure and flow of information.

## A Massive Dismantling of the Arts of Domination to Manipulate Humanity

These are the reasons why brainwashing, mind control and virtualised platforms of domination and manipulation are focused on the genetic system and its networks using electromagnetic tech. As part of the electromagnetic tech is a communication system that targets the electromagnetic fields of the earth and humans, it becomes clear that the electromagnetic waves, the ionosphere of the planets and the electromagnetic environment of the earth's core are being used "in the manner of heaven, human and earth".

This is the archetype of Jehovah, the chaos of Satan, the belief in ascension and the dimensional trap of ordering chaos, the programme of the four basic elements and the platform of the heaven of the fifth element, which the Hermetists have described as the structure of the universe.

It is introduced into the human device as a magical and analogous system of Higher Self, Middle Self and Lower Self, classified as software-middleware-hardware in software design, translated into the concept of boundary-controller-entity in systems analysis and has been expressed as the concept of view-controller-model in application development.

All elements have been implemented in a cyclical manner in the five-element flow formula. In other words, it is a spatial magic that is integrated and limited on the fifth elemental platform of heaven. In other words, it corresponds to the iterative or spiral development of information technology and is closely related to the "linear space-time concept" as the basic strategy of the ruling art. It is what is perceived as the futile effort or reincarnation system.

Installed in the deep consciousness of the Japanese are the Amatsukami, the Kunitsukami, the eight million gods and humans. Divided into electric clusters that rule the heavens and magnetic clusters that rule the nations, the conceptual programme is to combine the father tones of the heavens with the vowels of the earth to rule the children, the eight million gods and human beings. And this programme has long been operating both subconsciously and unconsciously through language forms and genes. It is no coincidence that the structure of this virtual environment is now manifesting itself.

This programme of mixing real stories and myths has been in use in the world for a long time. In other words, it is a traditional way of implementing and operating mind implants. The Corpus Hermeticum in the Hermetic tradition is well known. The Upanishads are similar, especially the repetitive installation with the similar terms, and the Zohar in the Kabbalah basically encodes information in story form. The same goes for the Bible, which is said to be the most published book in the world. So is the Babylonian myth of Enuma Elish.

In modern times, media and content have inherited this role. The business world, especially the marketing, uses stories extensively, and app development has evolved to implement user stories. This is context design in domain modelling and scenario development in dominance and manipulation. Recently, more and more people have warned that stories can ruin the world. This is especially true in terms of attacking on the subconscious and emotional manipulation.

This, like technology, is neutral to the story itself. This is because the identity of the story is the development of the application using the coding patterns and metadata of the genetic mind. It is a technology that encodes information that is difficult to remember as it is, and data that cannot be verbalised. In particular, when targeting humans, it is an application that targets the heart and mind.

It is therefore only a question of the goals and rules of the system and the mindset of those who use it.

The primitive translation in Babylonian mythology concerning this electromagnetic tech and its users is also interesting. It is the Anunnaki, such as Anu, Enki, Enlil, Marduk and Ninurta, who rule the heavens. And it is the Igigi, who are chosen as a cluster to rule the earth. It is interesting to note the proximity of sound to the Ninigi of Japanese mythology. Sound and light are the basic components of electromagnetic tech.

It is like recording sound vibrations on magnetic tape. So when light (electromagnetic waves) is applied to objects and instances in the subdomain region, sound is like a communication path that actively or consciously bridges between those objects, instances and sometimes resource pools. Sound also plays an important role in the installation of programmes such as mind implants.

This is the electromagnetic interaction between the seven celestial bodies of alchemy and the Earth, and the very reason why alchemy is also called the 'Art of Sound'. It has also merged the three components into one, implying a departure from a moulded (i.e. zombie) existence. These three components are the three selves, or body-mind-spirit, and the technological and systematic boundaries, expressed as the three sacred tools.

The platform that governs the human device that operates with these three components is the electromagnetic platform of the earth and the seven ancient celestial bodies. It is encoded in the seven spectrums of light, the musical scale and the mapping to the days of the week. Thus there is a history of inner and outer interactions, the flow programmes of the five elements, and the bandwidth limitations of information and vibes.

Alchemy has therefore open-sourced the process of dismantling, purifying and reconstructing all these techniques once and for all in all formats. The aim has often been to break free from the cages of the mind and the illusionary systems, some of which were intended to be on the dominant side of the illusion. And only a very small number of determined alchemists have hacked and disclosed information (via electromagnetic tech) with the aim of restructuring civilisation.

All alchemy books are similar and the approach is almost identical. However, depending on the combination of writer and reader, a 'Top Secret Portal' can be opened. This is because all the information is a similar system and at the same time consciousness and vibes change dramatically depending on which perspective is studied. Furthermore, since the books act as a login screen for the alchemical network, they are of little use if we cannot understand that it is a network system that transcends linear space-time. When you understand that, when you activate your genetic information, your genes act as a secret key.

Now you have three keys. So I want you to use these keys to open the top secret portal across linear space-time, enter it and develop a new civilisation and your own personal path of your own choice.

## Forge Your Own Path, Your Own Will, to a New Civilisation

So far we have used this book of alchemy as an anchor, as a portal to connect to the network, and as an API endpoint to access the detailed specifications of the technologies. Yes, you and I have been working together using the mind-level nanotechnologies of the Alchemy of Forms and the Philosopher's Egg.

From here, you enter the stage of developing your own path and integrating yourself into the 'New Era, the Best Civilisation' at your own will. Today, in July 2023, I'm not sure if only 3% of the world's population is ready to begin this great task of civilisation building. This is the status quo of the collective consciousness of humanity, 'a snapshot of our collective consciousness at a given point in time'.

I feel it would be an enormous task to change that. You must have felt the same way, and that's how far you've come. But now that the portal has opened, there is hope and there is a way. Practical and pragmatic alchemists like you and I know that opening portals and getting information is only the beginning. From here it is an evolutionary stage that requires a more sophisticated approach.

Planning strategy is not the goal of business. Release is the starting point for app development. Likewise, this book of alchemy means nothing if you don't smelt yourself into a philosopher's stone and release it into the world as a catalyst (for the new era). Stone smelting is fundamentally different from the eccentric spiritual industry, where you can alter timelines with mysterious work that only jumps.

I am currently experimenting with a technique that allows me to detect a form of communication that cannot be picked up by the five senses. This is a sound-focused technique, which is a slightly dangerous method of making potentially flowing data audible (for a moment) in the mind. What becomes apparent in such repeated hacking of my human device is the startling fact that in the electromagnetic tech on the planet, "data for the purpose of jamming techniques and mind control is constantly flowing through the network in the background".

It is perceived as a mass of whispers pouring in, and at the same time as a mass of metallic sounds playing disharmony. It could possibly be the electromagnetic energy flowing through the internet that humanity uses regularly, or the thoughts that people drop unconsciously, or it could be an attack using the low frequencies of the earth. I don't know what it is yet, but I am sure that there are 'multiple techniques to create disharmony and inflict pain'. I've never been very good at filtering the massive amount of sound and information coming in from the environment, so when it manifests, it's almost torture.

The first thing we have to realise is that we have to build civilisation prototypes out of this environment. We have to get to the hidden points that the existing spiritual and self-help industries deliberately ignore, and we have to understand things without distinguishing between positive and negative. This is exactly the same as the calm reaction when a server is hacked. If the server administrator says, 'I only accept positive tasks with high vibrations', the incident will not recover in the slightest.

It is also important to understand that we must change civilisation from within the genetic archetype and the spatial magic that protects it. Otherwise, it is easily carried and diverted by economic platforms and forgets its original purpose. Or, the will itself is manipulated by firewalls and routing.

If you are unaware of the existence of such multiple techniques and you start a vibrational upliftment or ascension festival, you will continue to implement the reverse leverage of "as long as I'm comfortable on the sacrifices of others, that's all that matters". Building a civilisation in this state of consciousness creates a situation like the present one. To repeat it is an iterative development, a flow programme and a futile effort.

So a fundamentally different approach is needed. It is a more scientific approach that renews the core of the philosopher's stone called ourselves with 'consciousness tech', activates the microcosm (local universe) called ourselves with 'electromagnetic tech', and from this catalyst penetrates the collective universe or multiverses with a new state of consciousness. It is not always comfortable for you or others. Especially if it is a disruptive innovation.

This approach is leveraged in that the action from beyond linear space-time and the action on linear space-time civilisation occur simultaneously. And it is designed so that the more philosopher's stones there are, the more exponentially the overall activation increases. This is the hidden strategy of the galactic alchemy.

Imagine this. The moment when you are freed from the pressure of being suffocated underwater and your mind is crystal clear. The feeling of seeing the world in a different light. The resurrection from the zombie state where you were 'just alive' or 'living dead' as if your individuated consciousness was dead. This is the smelting of the philosopher's stone, which begins with the quantum warding technique of the philosopher's egg.

An interstellar civilisation, using technologies far more advanced than ever before, building a civilisation in that state, having deep respect for all living beings and co-existing with them. What would you do if such a world awaited you in the future?
