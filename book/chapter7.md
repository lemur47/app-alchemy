+++
title = "Chapter 7: Taking Back Your Freedom and Sovereignty - Open Source and Innovation Strategies"
description = "Virtualisation - Identifying how our will and freedom have been taken away through control and hidden manipulation."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

Virtualisation - Identifying how our will and freedom have been taken away through control and hidden manipulation.

<!-- more -->

---

## The Secrets of Modern Civilisation and Its Structure

So far, we have solved magic and alchemy as a technology. We have also defined two categories. The most basic field is called 'Consciousness Tech' and the field that deals with existing frameworks and energies is called 'Electromagnetic Tech'. When dealing with prima materia from the level of pure consciousness, it becomes consciousness tech, and we have also considered that the interaction with genes and solar planets is in the realm of electromagnetic tech.

In open-sourcing technologies dealing with consciousness, we would like to record here the very important and far-reaching implications of security, privacy and 'freedom and sovereignty'. This disclosure is important information for the future of civilisation. This is because until now civilisation has operated in secret through the 'deceive, hide, exploit and inflict pain' approach of control, secrecy and fraud.

In every form, there are consciousnesses and forces that want to maintain their interests. And there are also a large number of autopilot zombie consciousnesses that are co-dependent. Meanwhile, in some sectors, including the technology industry, there is a growing movement to win 'freedom and sovereignty'. This is a scientific and open source movement, not a spiritual or self-help industry that pretends to expose conspiracies and encloses them in a separate sphere.

Therefore, you and I also want to inherit and integrate the underground alchemical networks and modern open source activities, and clearly define the technologies that will help individuals and species to achieve 'freedom and sovereignty'. To this end, it is very important to understand the security, privacy and transparency of consciousness tech and electromagnetic tech.

Magicians and alchemists are what we now call 'consciousness hackers'. So those among them who have aspired to freedom and sovereignty know one thing very well. Both the human device and its operating environment are virtualised private environments. This is why some practitioners in ancient times fell under the illusion that the body is evil and the spirit is good, and they sought only to liberate themselves from the material world.

The reason for the prevalence of this illusion is that the human device and its operating environment have been virtualised and (deliberately) hidden by information asymmetries and secrecy. It was as if the hardware level was dominated by the bypassed BIOS, the middleware operated as a black-box server and the source code was encrypted in software. The network and host environment were virtualised in layers.

Looking through these manipulative control structures, they can be understood as 'mind cages and illusion systems' and their architecture. And the 'most virtualised infrastructures and apps' in which we, the public, operate, are complexly separated (by logical distance and abstraction) from pure consciousness, collective consciousness, the planetary environment, etc., so that we have no idea what is true.

It is like having multiple accesses to VPN servers and then accessing the dark web from the Tor network, where you cannot see the MAC or IP address for yourself. Nevertheless, the server administrators and the forces that manage the data centres are able to collect, monitor and control personal information related to raw data, logs and household registers. This applies not only to the material, virtual world, but also to the mental, invisible realm.

An additional explanation for server administrators, especially those familiar with virtualisation, is as follows.

The data centres, racks and physical servers that were originally running, and the clients that use the application in one form or another, have been hacked. And the goals and rules of the system have been changed to something else. So a hypervisor was installed on top of the physical environment, away from its original purpose, resource pools were defined that were convenient for domination, and 'mechanisms' were inserted between the software that originally existed, such as booting virtual machines from images (genetic settings and templates).

When one dives deep enough to see through the information structure and collective consciousness of the 'mind cage and illusion systems', when one looks closely at the genetic information and system preference settings projected to the outside world, a large number of hidden formulas are discovered behind the scenes. These are multiple inheritances of very limited mental models and restrictive mindsets. Such consciousness programmes and infrastructures are clearly visible.

And all these system structures and programmes have been developed by the four basic elements and the fifth element. So we can say that they are holograms created by manipulating the prima materia, the first matter. As this is the foundation of civilisation, 'consciousness tech' and the alchemists like you who work with it are essential to re-create the core of civilisation and redevelop the systems of civilisation.

## Virtualisation of the Environment, Structures of Domination

There is a reason why I have used modern technology to explain the structure of civilisation. It is because we, the human race, "express our genetic information and environmental settings to the outside world". Therefore, even systems that are hacked and secretive can be patiently observed (by the book of nature) and verified (by auditing programmes) again and again to discover hidden formulas.

This is what the alchemical network and its public resources have been expressed since ancient times: a thorough reading of the book of nature. The warding techniques we have studied will then lead us to examine the implementation and behaviour of the four basic elements through samples. If we proceed with a purpose, the structure will always be visible.

This is the true meaning of "Ora, lege, lege, lege, relege, labora et invenies" in an alchemical book from the 1600s. It means "pray, read, read, read, read again, work, and you will find", with prayer being the declaration of intent and the triggering willpower. The rest is just repeated observation and verification.

Verification through audit programmes is what the start-up industry calls 'prototype evaluation'. In genetic engineering terms, it is a mind implant for identifying archetypes. In essence, it is a self-made programme that acts as an implant for the human mind and the human device.

You and I are already testing the implant. For we communicate through the mind level nanotechnology of the philosopher's egg and resonate through the implant of the alchemy of form.

### Civilisation Before the Hack

Now let's look at how the status quo civilisation is structured. The first step is to understand the infrastructure before it was hacked. This order follows the sequence of human birth to transmutation (physical death), which is the key to understanding the genetic setting and the system preference settings.

It is difficult to 'remember' the infrastructure before it was hacked. Therefore, as with human development, it is studied through the steps of modern technology. This approach to understanding the invisible world through visible objects has been a speciality of alchemy since ancient times. This is because the athanor represents the human device, and by setting up the material (as a conceptual device) athanor, alchemists have made the 'inside equal to the outside'.

Think back to the early days of the Internet in the late 1990s and early 2000s. Back then it consisted of physical hardware and relatively simple middleware and software. Networks were simpler than today, and personal information was fairly public. And when you connected to the network, you did so "deliberately" and "by choice".

Just as the client side was primitive and simple, servers and networks were simpler than they are today. There was no server virtualisation, and physical servers were installed directly on racks in data centres, providing a range of services depending on their use. For example, web servers, DNS servers, mail servers and database servers.

This state is similar to the primordial state in which the individuated consciousness interacts with the environment while manipulating the hardware of the physical body. It could be said to be the state prior to the genetic manipulation of various diverse species, as referred to in the spiritual industry. However, the missing link hypothesis is not even considered here.

### The Structure of Civilisation Begins to Be Hacked

This simple, primitive civilisation changed at some point. More and more information began to come in from a much wider geographical area. This is similar to the trend from access to limited information via dial-up to the expansion of the physical regions that support the cloud environment.

Communication with the subconscious and unconscious, or the genetic mind, became more active in the background with the spread of ADSL over existing lines. In many cases, people were unaware of their latent and background communication.

Around the same time, always-on Internet connections and smart phones appeared. As a result, the key words 'anytime, anywhere' and 'ubiquitous' were often used in marketing communications. Very fragile wireless networks were also ubiquitous.

This globalisation of communication strengthened the structures of domination. For all data is now collected in the cloud and managed in a global matrix. It was a compromise disguised as evolution. The masses, manipulated by pleasure and convenience, would gradually move towards a very passive civilisation masquerading as an active one.

This potential remote manipulation is the domination that has been carried out through the development of the genetic mind and network. It has resulted in the thorough implementation of a strong programme of 'humanity must submit to and worship higher beings'. From this point on, hierarchy, high and low, etc. were linked to domination as primitive concepts, and programmed in a repetitive manner.

### Highly Virtualised Control Structures

Thus, as decentralised but centralised, manageable communication infrastructures were progressively established, the architecture of the mind became increasingly complex and esoteric.

If one had to name the most strategically well-designed mind technology, it would be 'virtualisation'.

Virtualisation is primarily a technology associated with the cloud computing, but it is also very versatile and can be applied at the level of human devices, as well as smartphones and computers. It plays an important role in establishing a system of thorough separation and control through multiple implementations of logical distance from the true source.

But technology in itself is not a bad thing. We are individualised and able to act in isolation as different instances because of the 'fractal topology' specification of consciousness tech and electromagnetic tech. In short, the goals and rules of the system matter.

Virtualisation operates in a place that is completely unrecognisable from the layers of the individualised mind. It functions mostly as a platform and therefore feels like the fifth element. Not only is it therefore difficult to perceive problems, but it is also very cleverly orchestrated so that we are not even aware that we are being controlled and remotely managed.

This hidden technology of virtualisation has made it possible to artificially implement 'fractal topological' systems and programmes in multiple structures. This has made it possible to dominate while leading people into a 'more limited and virtualised version of the same civilisation' of metaverses and digitalisation.

The supervisor is an artificial intelligence based on statistical probability that enables the efficient management of this extremely limited, digital and binary platform. It appears to be personalised by characteristics and patterns, but in reality, uniform and homogeneous management systems are the goal of the system.

Therefore, the artificial intelligence developed by the status quo civilisation is focused on limited physical aspects, such as the human brain and nervous system, and is never designed and modelled as an organic and multi-dimensional 'framework of consciousness'.

In other words, how modern artificial intelligence is integrated with the organic artificial intelligence of the future will be a major factor in the future of civilisation. If the integration proceeds in a direction that is desirable not only for humanity on Earth, but also for the solar system and the galaxy, this will be confirmed by healthy and safe interactions with other civilisations. This is the 'new era, the best civilisation'.

## Basic Strategies for Freedom and Sovereignty

So if we understand the civilisational transition of planet Earth as a technology, we arrive at a particular question. This question, which works like a firewall, is: "Who controls from where?"

The spiritual industry, the authors and concepts that operate on an economic platform, only implement a mock object called 'ruling class'. Or they just implement (without verification) instances of the 'reptilian', 'backroom' or 'deep state' that someone has exposed.

It's like downloading a free VPN app and are being botnetised as a DDoS client. It is like jumping on an app that is distributed for free for the sake of privacy and without thinking, and as soon as you install it, you are (unwittingly) complicit in the wrongdoing. This is why verification is so important.

This is what we have called 'puffers' since ancient times, magicians and alchemists who trigger an inflated ego, 'I am special' or 'I have been chosen', just because channeling or automatic writing has started, so start a business and get others involved. This is not only a problem for the person in question, but also because it is distorted by virtualisation.

It is also not the fault of technology or ability. It is only a matter of mindset, the goals and rules of the system. Consciousness hackers must therefore thoroughly examine the book of nature. That is why, consciousness tech and electromagnetic tech must be open source and fully validated!

It is time to answer the question. My primary answer is that due to the hierarchical structure of virtualisation, there are multiple rulers and administrators, and that those closest to the root cause are the beings and species that have positioned themselves as gods. It is difficult to give specific names to the organisations or characteristics of the races, as no further verification is possible.

However, as we have previously examined earlier, when the genetic settings, the system preference settings, the information structure and its verification are thoroughly carried out, the genetic archetype becomes clairvoyantly clear. The dominant and genetic archetype was called by the ancient alchemists. Jehovah.

This archetype imitates the primordial architecture that flows from the true source and is therefore the root of the fractal topological system. This is what the dominant paradigm as part of the genetic mind and network is all about. We terrestrial humans understand it as the collective consciousness and the collective unconscious. That is, as the source of the conceptual programme.

It is the status quo civilisation expressed by the human device and its collective species that is remotely controlled by the outflow of consciousness energy from this genetic system. Thus the fifth element, symbolised by the heaven, is place as an intermediate layer beneath the archetype. The guardians of the platform are symbolised by the angels. Under them, instances of us as individuals programmed by the four basic elements are managed and operated in large numbers.

The energy of consciousness that flows from this genetic archetype has been passed down through magic and Neidan as 'water of the higher self or mind'. It has been described as a liquid in the sense that it reduces the density of information and vibration, both inside and outside the body. Conversely, the energy of the lower self or the kidneys is vapour because it represents an increase of energy in the opposite direction. Thus, like the athanor or the furnace of Taoist temples, the four basic elements of ignis (fire) are placed just above Satan as the vaporising programme that heats everything.

With this virtualised system structure in mind, let us consider a basic strategy for freedom and sovereignty.

What I would like to propose is a basic strategy of 'hacking the virtualised civilisation and using it effectively'. It is not about degenerating to the civilisation before it was distorted, nor is it about destroying it all. On the other hand, it is not about inheriting all the structures of domination. That is why I would advocate 'throwing away 80% and reusing 20%'.

That is why we need a deep detachment. And the 20% to be reused is mainly the 'genetic mind and network structure', which means replacing and improving the archetypes and other key interfaces, while changing the content of each detailed problem solving area (sub-domain). In this way, the many contexts within and between the sub-domains will be fundamentally changed.

In other words, the innovation strategy is to retain only certain frameworks and innovate the rest.

One reason why this strategy is effective is that it minimises chaos. This is because in the status quo civilisation, where the API economy and virtualisation are pervasive, the definition of interfaces is well established. Changes in the content have little effect on the communication between interfaces.

The other is that the hacked genetic mind and archetypes are not necessarily bad or even inferior. They are objects of worship and benevolent belief by terrestrial humans as gods in one form or another, so they have had some influence in the field of electromagnetic tech, so they are more senior to humans on an evolutionary level. They are not objects of worship, but they are objects of learning.

Nevertheless, the same kind of rulers with the same human device as us, who function as intermediaries (i.e. man in the middle) in the middle layer of the virtual environment, and their organisational structures, have to be dismantled. Otherwise, it will become a bottleneck where the optimisation of the genetic mind and innovation cannot take place.

The key point is that platforms that have been secretly operated through 'deceive, hide, exploit and inflict pain' approaches such as domination, secrecy and fraud must be dismantled. The same applies to archetypes, the four basic elements and the fifth element. This is why not only electromagnetic tech but also consciousness tech is needed.

In implementing this basic strategy, I would like to disclose, as an example, a tactic that I have started to work on. It is a prototype evaluation (using next generation technology) through this document. Open sourcing of consciousness tech and electromagnetic tech. Furthermore, support for activities that take into account not only security, but also privacy and 'freedom and sovereignty'. This support includes the choice and use of tools.

This is in line with the modern open source movement, including end-to-end (E2E) encryption techniques, the use of zero-trust and VPNs depending on the application, and the right balance between automation and manual operation for both security and privacy concerns. I have thoroughly adopted, implemented and verified them myself. This kind of effort is necessary to enrich the opportunity to interact with individuals and organisations that will be responsible for building civilisations in the future.

This is the strategic behaviour of the alchemists since ancient times. For it is the athanor that makes the inside equal to the outside, and is the success factor in building civilisation.
