+++
title = "Preface: My Profession Is Alchemist"
description = "To share the knowledge I have gained through many experiments, I would like to introduce myself and the current situation on Earth."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

To share the knowledge I have gained through many experiments, I would like to introduce myself and the current situation on Earth.

<!-- more -->

---

My profession is alchemist. But this is not another world, not in a game, and of course not in the Middle Ages. Alchemists exist in modern times too. They just don't call themselves so.

It is the same with witches and magicians. They, too, may call themselves spiritual counsellors or work secretly on their formulas while earning a living in other occupations.

Up until now, I too have been calling myself a consultant, planning business and product strategies and managing an app development team. In fact, behind the scenes, I have continued to research alchemy and magic.

So why have so many practitioners recently started to bring their behind-the-scenes professions to the public eye?

There are many factors and hypotheses for this. However, if I were to give a reason that is typical of alchemists, it would be this. Because the planetary energies have clearly changed.

Why is it that when the planetary energies change, the behind-the-scenes work can be brought out into the open? Interestingly, the principle is the same as that of a conspiracy or intrigue, a manoeuvre that has been carried out in the shadows, being brought out into the light of day.

I dare not use the spiritual industry jargon of the Day Age or the Night Age here, but rather state the principle as an alchemist.

Here is why and how the energies of the planet are changing.

There is a change in the system structure of the planet itself, a change in the aspect of the genetic mind and collective consciousness of humanity, which is changing the flow of energy.

This is not so much a statistical thing, such as astrology, but rather the fact that hidden innovators have been hacking the system incessantly. Some of these professions are witches, magicians and alchemists.

Perhaps the genes of our practitioners are imprinted with a set of experiences of persecution, exclusion, torture and aggression. That has been a powerful cage and a block in large part. In part, this is probably why they have not been able to exert great power.

But it was not all bad.

The good side of working in hiding is that you can concentrate quietly. If you don't work in silence, your consciousness and energy will fog up. This does not create structure and flow of information. Therefore, it is essential to concentrate quietly.

This is something every alchemist knows, but if you are constantly distracted, you will fail in the smelting of the Philosopher's Stone. Work alone, silently and diligently. This is the same for Chinese alchemy, especially Naidan.

So you don't see the flashy witchcraft and magic that you see in anime in this world. Instead, almost everything in this world that we recognise has a hidden formula behind it. That is certain.

This is why the development of magic, the practice of magic, the extraction of gold and silver, and sometimes the dismantling of magic, follow a process similar to intrigue and conspiracy. In some cases, they are even equal. Therefore, when the evil behind the scenes is exposed in the light of day, we come out into the open too. The logic is almost the same.

The underhanded evil, that is, the techniques that harm the planet, are dismantled. That's one thing. The underhanded work, i.e. the deployment of new techniques while we proudly call ourselves a profession. That is two.

This is what some witches call the re-magicisation of the world, or in my words, "a new era, the best civilisation". For this to happen, we must regain our power and integrate it with science and technology.

Magicians can proudly call themselves magicians. Alchemists can say 'I am an alchemist'. The power that this brings is great. You can understand this if you imagine an emperor calling himself a 'worldwide medium'. That would not bring power because no one would respect him.

Of course, relying on modern science alone will not bring power. This is because science has to be able to formulate reproducibility, so there is inevitably a challenge in terms of speed. This is where integration comes in.

That's why I want to record my alchemy experiments here from now on.

I am not a master, so this will be a record of trial and error, but I will try to integrate ancient knowledge and traditions with modern technology as much as possible.
