+++
title = "Chapter 8: Dismantling the Hidden Magic Formulas - The Technology That Keeps You From Sovereignty"
description = "Like bending reality, why have your magic and efforts failed so often? One of the root causes is a hidden technology and well-designed systems."
date = 2023-07-04
draft = false
template = "book-content.html"

[taxonomies]
categories = ["book"]
+++

Like bending reality, why have your magic and efforts failed so often? One of the root causes is a hidden technology and well-designed systems.

<!-- more -->

---

## The Real Reasons Why Your Efforts Have Failed

So far we have explored the 'consciousness and energy' of the One and the First Matter, and we have understood the programme specifications of the four basic elements. We have also understood the fifth element, a platform that transcends linear space-time. Until now we've only deciphered the classics and folklore, but you and I have gone further.

We understood the most basic and universal technologies that focus on consciousness and energy as 'consciousness tech', and classified technologies that manipulate already created things, such as genes and interactions with the environment, as 'electromagnetic tech'. This has allowed us to hack the status quo civilisation and the structures of domination through the sophisticated fraudulent art of virtualisation.

From here, we look further into the abyss. We do not recommend that you read this document out of curiosity, as a significant gestalt collapse awaits you. If you are not prepared to look into the information, it will easily break your mind.

I'm sure you've been there. No matter what you try, no matter how much you stick to the basics, no matter how long you keep at it, you experience that your efforts are completely fruitless. The reality that all magic and alchemy has failed at every turn. The truth that the same pattern of jamming techniques has been invoked, as if the whole world were the enemy, as if solid self-improvement were a mockery.

Many spiritual leaders and dogmas would have preached it as 'self-responsibility' or 'original sin'. That you are responsible for everything and that your inner self is just projected onto the outer world. Because you have that vibration within you, you simply resonate with the same vibration in your environment. Alternatively, you have been encouraged to avoid responsibility by saying that it is not your fault but the fault of the social structure.

And as the habit of avoiding responsibility and 'covering up the stench' escalated, they would have made certain secret societies and symbols virtual enemies, denouncing all responsibility to secret societies (of which they had no knowledge) and organisations (with which they had no involvement), and channeling their conscious energy in the opposite direction. And without exception, the leaders who instigated this were prominent on the economic platform.

Unfortunately, they themselves have not identified the real causes.

They do not understand the system or the technology. Nor have they been involved in any hidden conspiracy projects. They just parrot and repeat what their teachers and forerunners have preached. The same is true of the spiritual and self-help industries, where dead templates have been used for a long time, including topics related to money and energy.

I have previously described the phenomenon as a botnet that joins a DDoS attack the moment you download a free VPN app out of concern for your privacy. To use the recent news analogy, it is like a social media app where 95% of the users are automated programs or bots.

The same goes for information companies that steal other people's content and distribute it without verification. It is as if the pirated software is infected with malware, which causes a lot of trouble not only to one's mind and mental data, but also to the global network. It is a zombie resource for remote control, an autopilotable bot, and is widely used as a common tool for propaganda.

Their teachings that seem right at first glance. Their methods that seem somehow reproducible. Surely 'some' of it is right. But not all of it. It is just an old generalisation, and therein lies a great trap. Just because some of it is true, there is no end to the cases where it is preached as the universal laws and fast-tracked business. And the industry that deals with the invisible world has developed to the point where it is allowed to say anything, regardless of whether it is true or false.

Despite this crisis situation of rotting collective consciousness, some of the root causes have finally been uncovered.

The real reasons why your efforts have failed. The root causes are not determined by such generalisations, but by various 'personalised' reasons. This is the first truth. And, as we have written, the 'elitism and mediating rules' that have developed throughout history are at the root of it. And finally, that it has been cleverly leveraged by technology. In other words, multiple root causes and detailed causes are intertwined like a hologram.

This is the 'dark data' of a statistical, probabilistic civilisation that has sought a uniform and homogeneous management system. This document artificially deconstructs the leverage that has been cleverly concealed. At the same time, it implies a gestalt collapse of elitism and mediating rules.

Why do we have to dismantle this huge and corrupt art? It is, as we have explored together. It is to regain our 'freedom and sovereignty'. In order to regain our freedom and sovereignty, we must take back the responsibilities we have abandoned and the energy that has been stolen from us. To do this, we must be prepared to shut down the system once and renew it.

## Dismantling of Elitism and Mediating Rules

A huge amount of verification is required to identify the causes of personalisation, including detailed genetic programmes, tribal characteristics and the system preference settings of the human device. It is not possible to generalise and simplify further and say that "90% of the time this is OK".

So in this document I want to explain two points that have more in common. That is the platform and the strong leverage behind the root causes. One is elitism and mediating rules, which is one of the biggest leverage points: the goals and rules of the system. The other is the strategic technology in the information wars, the "firewalls and routing", which are central to the communication platform: the structure and flow of information.

The first dismantles elitism and mediation rules. This is a very powerful leverage because it is the root cause that defines the goals and rules of the system. This is because the goal of the system is 'domination and manipulation' and the rules are defined to achieve this domination and manipulation. In simple terms, it is the 'business rules' that define the architecture of the platform.

In other words, it is the 'logic' of this virtual world. Normally, it is impossible for users in the virtual world to go outside this logic, and there can be no implementation beyond the business rules. Of course, no services are provided outside the logic. This is the problem-solving domain of the system, a 'domain' set up for the purpose of domination and manipulation. It is the 'warding art' of the virtualisation technology.

A more detailed analysis shows that the billions of users managed by the boundaries run on rails laid according to a defined context within the domain. This is the scenario. The mass integration of these scenarios is the virtual universe. A unified verse (i.e. a universe) refers to a single universe created by the integration and unification of multiple scenarios.

This is the true nature of the 'mind cage and illusion systems' and why there is no escape from illusion when one attains Cosmic Consciousness.

Due to this system specification and hidden leverage that no one has clearly defined, all kinds of information have been complicated, and the five element programme has been repeating a sterile loop with no exit and no end, like an Ouroboros, where the five element programme repeats the flow and rotation through compatibility and conflict. The conflicts and transitions of information or teachings in the spiritual industry are loops caused by the magic of this flow programme.

Therefore, the spiritual industry is part of the 'mind cage and illusion systems' that function like a rat race due to the flow programme. It is designed at the platform level to trigger the desire to satisfy the mind and to cross-sell teachings and people. It works like the paradigm shift advocated by Thomas Kuhn. In other words, it is no different from economic or tech trends.

Why is such a diversionary programme in operation, running the spiritual industry as a virtual domain of the economic platform? When you set up questions and issues in this way, a large number of formulas emerge. This kind of effort by you and me is hacking, the 'backdoor development to the outside of science' that determined alchemists have been working on since ancient times, risking their lives.

Gold can only come from gold. People can only come from people. The garbage-in, garbage-out alchemical order contains such multiple meanings. It is made unreadable unless you know the technique of holographic unfolding of language and decoding it in three dimensions. It is not the fault of the encryption, but of the consciousness and vibes of the reader.

In other words, it is impossible to achieve goals in the virtual universe that are not supported by business rules and scenarios. Therefore, the thesis that you are the creator and that everything is possible is a total lie, although it is effective as a fraud. This is because the genetic mind and the virtual system that you recognise as the universe are not designed or implemented as such. However, they continue to cause very serious problems because only the words are true.

Behind these botnets and zombie networks is elitism and mediating rules.

Elitism is easy to understand in terms of control and manipulation. But how that elitism is connected to mediation rules is an easily overlooked point. The mediums are gods, emperors, royalty, nobility, shamans, priests, priestesses, gurus, magi and masters. More specifically, they are teachings, traditions, oral histories, dogmas, cosmological theories, philosophies, psychologies, frameworks and multiple warding techniques.

Elitism in this document is, in a nutshell, 'an ideology for the benefit of a particular race or group of beings'. It is the strongest lever of the mindset (mind universe) on which the goals and rules of the system of domination and manipulation are developed. It is as if Amenominakanushi in the Kojiki is the idea, and the pillars under Takamimusuhi are the leverage points (models) of the system. Alchemically, the system is based on 'consciousness tech', where energy is manipulated by consciousness. In other words, the hidden truth is that this virtual universe is constructed by magic and alchemy.

In short, the most basic programme of brainwashing and mind control is the imprinting that "only through an artificially constructed virtual environment can we survive and evolve". This virtual environment is reinforced by the logical distance of 'multiple hierarchical dimensions' and multiple warding techniques, and is robustly implemented so that it cannot be easily breached. This has evolved into a belief in ascension and a programme of worship of higher dimensional beings.

Everything is a similar system and is in the same flow because it inherits its system structure. The economic platform, the spiritual industry, the CEO mindset and the cosmological theory are all fractal topologies. This 'cycle of similarity-based flow programmes' is the basic specification for creating illusions based on truth, looping the basic scenario of 'linear space-time' as if tracing the grooves of a record version of a planet floating in space. It functions as a cage for the mind and is established as a common perception.

Thus, the programme has been implemented over and over again into the genetic mind of humanity that in order to connect with the True Source, a 'path' is needed, a medium is needed, and it must go through the gods and Christ. In truth, it should have been the kind of technology and system, more specifically a service, to be used "as a trigger by people who are hard to connect with". Channellers and psychics have also inherited this structure.

This is why determined alchemists have been trying to make it open source since ancient times.

## Uncover Hidden Firewalls and Routing

We now move on to the dismantling of technology. This dismantling of technology reveals some of the root causes. It does not reveal all the root causes, but it is a remarkably cleverly implemented and sophisticated technology.

The mid-level lever of 'information structure and flow' in systems thinking is analogous to 'Tiphareth' in the Kabbalistic Sephiroth. It can be seen as the equivalent of the 'Kuninotokotachi' pillar in the Kojiki. It can also be seen as an mystical interface, the Marduk of the Anunnaki in Babylonian mythology. They are electromagnetic interfaces.

The virtual universe governed by their basic model and system is electromagnetically related to information strategies, uses feedback loops, acts on sensations and emotions, and manages vortex-like energies that project holograms.

In other words, they are 'manifested rules' that implement the goals and rules of the system. It is therefore a 'beauty' that embodies the rules, but it is also an 'infrastructure' related to the human device and the mind system, and is perceived as the 'platform' of the country. It is also seen as the heart at the centre of the hologram. To put it in alchemical terms, it is a technology that belongs to the (invisible world of) 'electromagnetic tech', which studies the sub-domain of genes.

Incidentally, the four basic elements and directions, i.e. the four winds, are mapped by this mind-based technology, which works as part of the electromagnetic tech. In other words, it is hacking the electromagnetic field or grid of the Earth and hacking the human device. So there must be some technology and a base near the Earth's core. Because technologically it has to be there.

It is part of the 'development platform' that traditional magic relies on. In modern technology, it is like a cloud provider. And the platform is directly the cage of the mind and the vessel of the illusionary systems. At the core of the platform is the genetic mind, the realm that humanity classifies as the collective consciousness or subconscious and unconscious.

Firewalls and routing manage this multi-dimensional communication network. The technology can also be seen through the lens of what the Kabbalah calls 'the linking of Tiphareth and Yesod'. In systems thinking, it is as if "the structure and flow of information is linked to the latency of the system". In modern technology, it is a zero-trust firewall, TLS decrypting encrypted communications, monitoring protocols and data.

In other words, if you or I are using a human device and are still registered as a device on the Zero Trust platform of domination and manipulation, our psychic heart and mind level communications are being monitored and forced to be routed (distorted) in a way contrary to our intentions. This is what the automated reversal technique is all about. How can this be self-imposed?

If this technological reversal is self-responsible, it is tantamount to saying 'you are responsible for being born with these genes'. This means that we are being persecuted for 'self-responsibility for being born'. The spiritual industry routes this into the uniform and fluffy expression 'you chose to incarnate in order to participate in the ascension event'. Isn't this a facilitation (marketing) of an illusionary system dressed up on the basis of truth?

Now, I will try to describe how this firewall and routing works as a technology. If you have a technical background, this should at least give you an idea of the root cause of the problem. Identifying the problem will prevent you from continuing to invest in pointless solutions and failures like I did, and as a consciousness hacker you may open a back door to 'outside the box'. I would then urge you to open source the specification.

First, surprisingly, this firewall and routing are closely related to the human device. So it is not a fire across the river or a war in a distant land. Because it is 'electromagnetic tech', that seems to be implemented in the form of implants in genes, hearts and minds. The reason for expressing it in a hypothetical system is not because the implants themselves were actually visible, but because they were understood as such after a thorough examination of the data flow.

This is exactly the same as the Zero Trust VPN, DNS, firewalls, etc. that are not visible. The apps are inserted as agents (implants) and the data is manipulated by them. It is actually the firewalls and routing that control the structure and flow of information. As a meander, the 'new era, the best civilisation' of the future will be a civilisation that (deliberately) implements this kind of mind technology on its own.

This quantum agent programme, or implant in the human device, will route the communication via the genetic network and the invocation of the will on its own, and depending on the content, it will be blocked. Not only are they blocked, but they are also reversed, feedback disappears or the situation is programmed to get worse.

What is superficially perceived is the phenomenon of wishes not being fulfilled at all, or the result of magic failing, or an attack where some kind of alchemy is interrupted in the middle, or it takes the form of harassment by botnets. It was understood that routing tables and firewall rules existed because they tended to be biased towards only certain areas.

In technical terms, packets are dropped incessantly, all logs are sent to '/dev/null', certain ports are not allowed in or out, and malware-infected devices get closer. This is related to what is happening to heart and mind systems, which is a huge disruption to daily life and business.

How is this technology implemented when it is leveraged as a platform for domination and manipulation? It is installed in your deepest consciousness as a prayer or doctrine that necessarily requires an intermediary. So the prayers (declarations) go through firewalls and routers and are all automatically processed in an unintended way. Wherever the energy goes, it is collected for processing by the system owner and limited stakeholders.

Even if the person is not aware of it, the data is sent out to the network in the form of a prayer, such as 'through [the interface] may this wish be fulfilled'. It can therefore be dropped, reversed or feedback deleted without your consent. This is a real crime against free will, but it has been ignored by the argument that it is your responsibility to download the client certificates and install the app. This is called fraud.

It is impossible to solve such technological problems with 'shadow work' or 'inner child healing'. Recently, some people in the self-help industry seem to have realised this, but for some reason they have been redirected (routed) to brain science and AI. I have observed that this is probably also a five-element diversionary programme.

So the structure and flow of information is beautifully and cleverly managed by firewalls and routing, from the level of individual life to the large-scale environment of the social and economic platform. It is an infrastructure that transcends even linear space-time, with quantum agents at both the genetic and individualised mind levels, and can be centrally managed in the cloud, which is called 'heaven'.

It is a ridiculously advanced technology, a powerful magic of the divine age, a galactic alchemy. It seems to me that there is a battle going on underneath for this infrastructure and system, or platform.

This is how an advanced civilisation can be built using consciousness tech and electromagnetic tech. It should be understood that there are tremendous possibilities. However, if it is used in the wrong way, as it is today, it is more than just reverse leverage. This, I believe, is why key authentication through consciousness and vibes is required as criteria for participation in the alchemical network.

The immediate task is to dismantle this mind implant. Specifically, to turn off and remove the client certificates. This is a very tricky part of electromagnetic tech, and the implants themselves cannot be uninstalled. This is because it is deeply embedded in the human device and collective consciousness, including coding at the genetic level. The only way to do this is to change the settings, hack the implant for another use, or disable the technology.

How to do this successfully is the mission of modern day alchemy.
