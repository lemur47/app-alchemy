# Human Mind System

This document describes how the individual components of the mind are interconnected as a vast system.

## The Illusion Systems and the Genetic Mind

![The illusion systems and the genetic mind](../assets/images/diagram-art-illusion-systems.jpg)

### The Illusion Systems
The illusion systems are hosted by some of the virtual universes that create, maintain and integrate holographic illusions with humans and other entities.

If a particular illusion is a particular mindset, then the illusion is made up of many mind cages, which are mental models. This means that the illusion systems are partially integrated with each other based on the core illusion built into the virtual universes.

### The Genetic Mind

The term "genetic mind" is taken from the Wingmakers.com materials. And this huge component of the human mind system seems to be well-integrated with the illusion systems.

Since the illusions are mainly based on our mindset and mental models, the genetic mind plays a vital role in maintaining each illusion. The genetic mind is like a vast platform of Git repositories, storing and providing countless mind modules that define the way we think and act.