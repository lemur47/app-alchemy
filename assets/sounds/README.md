# Sound Art

## Yggdrasil Protocol for Vital Life Force

Your tree of life is a branch of the planetary tree of life. When you reconnect with the Yggdrasil protocol, vital life force naturally flows into your life.

- [Listen on SoundCloud](https://soundcloud.com/lemur47/yggdrasil-protocol-for-vital-life-force)
- [Download this sound art](yggdrasil-protocol-for-vital-life-force.mp3)
- [Watch a conceptual video](https://customer-4nz1yudgna7bzdtr.cloudflarestream.com/45dcdd6754f7d1b706f8b64fbb20ba74/watch)
- [Sound blueprint for yggdrasil protocol for vital life force](yggdrasil-protocol-for-vital-life-force-blueprint.mp3)
- [Visual blueprint for yggdrasil protocol for vital life force](../artwork/art-yggdrasil-protocol-for-life-vital-force/README.md)

## Water Soluble Information Imprinter

Your good vibes are sent and imprinted into the water. Just as you play a water guitar with your tone, your body and the nature around you resonate, vibrate and are attuned to something good.

- [Listen on SoundCloud](https://soundcloud.com/lemur47/water-soluble-information-imprinter)
- [Download this sound art](water-soluble-information-imprinter.mp3)
- [Sound blueprint for water soluble information imprinter](water-soluble-information-imprinter-blueprint.mp3)

## Etheric Creation Process

You have the ability to derive the objects you wish to form from the etheric substance and vibratory platform. This process of creation allows you to launch a tangible holographic reality from mind modules as a blueprint. The key to successful creation is attunement.

- [Listen on SoundCloud](https://soundcloud.com/lemur47/etheric-creation-process)
- [Download this sound art](./etheric-creation-process.mp3)
- [Visual blueprint for etheric creation process](../artwork/art-etheric-creation-process/README.md)

## Celestial Cloud Communicator

Entities communicate with nature as the cosmic system environment through mantric vibration. Clouds in the sky have an interface to send and receive information energy in the etheric realm. You can also implement the interface to use the system environment.

- [Listen on SoundCloud](https://soundcloud.com/lemur47/celestial-cloud-communicator)
- [Download this sound art](./celestial-cloud-communicator.mp3)
- [Visual blueprint for celestial cloud communicator](./blueprint-celestial-cloud-communicator.png)

## Electromagnetic Wind Generator

Designed as a sound blueprint to explore how we can manipulate  vibration and light with our minds for creation. This experiment is expected to discover a new technology.

- [Listen on SoundCloud](https://soundcloud.com/lemur47/electromagnetic-wind-generator)
- [Download this sound art](./electromagnetic-wind-generator.mp3)

## Retune This Wold with Pureness

It's time to dismantle the unnecessary energy that is holding you back. Go back to the invisible centre where your consciousness resides, and then you'll see things clearly to start again.

- [Listen on SoundCloud](https://soundcloud.com/lemur47/retune-this-world-with-pureness)
- [Download this sound art](./retune-this-world-with-pureness.mp3)

## Mercury Song - Hermetic Dance

Your consciousness is gradually changing. Take your mercury and make it change. Then you will see things clearly. Feel the vibes coming from Mercury.

- [Listen on SoundCloud](https://soundcloud.com/lemur47/mercury-song-hermetic-dance)
- [Download this sound art](./mercury-song-hermetic-dance.mp3)

## Into Your Inner Sacred Space

You can go into your place anytime to stay away from electromagnetic noise and overwhelming situations. This world pushes you harder to keep moving without contemplation, so you invoke the properties of information energy to optimise your mind modules.

- [Download this sound art](./into-your-inner-sacred-space.mp3)
