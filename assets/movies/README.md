# Movies
Movie files are too large to store in the repository, so we link to our shared drive hosted by Proton Drive. The licence is the same as this repository - CC BY-SA 4.0.

## Conceptual Video Contents

### [The concept of space-time](https://drive.proton.me/urls/VXG8GCHME0#LOz2e0kzvz4g)
This video silently describes how you can expand your perception of space-time. To free yourself from the constraints of linear space-time, you can watch this video and then you will understand that space and time are non-linear in terms of their fundamental specification.

### [Electromagnetic Wind Generator](https://drive.proton.me/urls/HZEXJ609MC#HnjMouJ20C4P)
This sound art expresses the possibility of creating things from scratch with your mind. A mixture of contemplation, meditation and attunement enables you to create an order out of the virtual chaos that consists mainly of the genetic mind and etheric substances.

### [Yggdrasil Protocol for Vital Life Force](https://customer-4nz1yudgna7bzdtr.cloudflarestream.com/45dcdd6754f7d1b706f8b64fbb20ba74/watch)
Trying a new way of communicating with you through sound art. In order to adapt to environmental changes and the increase in information density, it may be useful to use new methods other than language. And this sound art with images has a lot of coded information energy in terms of cosmology, alchemy and technology.
