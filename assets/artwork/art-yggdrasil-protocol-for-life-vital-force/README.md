# Artwork - Yggdrasil Protocol for Vital Life Force

Your tree of life is a branch of Yggdrasil.

In the world of consciousness everything is vibrational fractal and structural topology. This means that your mind system is in resonance with the macrocosmic mind system. When you consciously integrate and connect with the macrocosmic Yggdrasil, the vital life force, which is information energy, flows into you. This is called abundance.

- Watercolour pencils
- [DreamStudio (Stable Diffusion)](https://dreamstudio.ai/)

## Creation Process

This artwork was created by lemur47 using Stable Diffusion.

1. Make a conceptual art with watercolour pencils
2. Send Stable Diffusion the rough sketch with a prompt
3. Review and select the output for a blueprint

## Final Version

![Final version of etheric creation process](art-yggdrasil-protocol-for-vital-life-force-final.jpeg)

## Concept Design

- [Conceptual design for input to AI](art-yggdrasil-protocol-for-vital-life-force-concept.jpg)

### Prompt

> Yggdrasil sending and receiving vital life force with the 9 realms, imaginary, psychedelic, surrealism, high detail
