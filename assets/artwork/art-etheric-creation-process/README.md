# Artwork - Etheric Creation Process

The process of creation with your "pure" thoughts and feelings.

To create tangible reality, you first design and form etheric reality in the realm of high information energy density. When you build up etheric objects as precisely as possible, you have a concrete blueprint as an alternative reality. Pureness is the key to success.

The same goes for communicating with nature. If you want to interfere with the current climate, first attune yourself into the environment with pureness. Then you send a short, simple and precise request to the environment. Simplicity and pureness are the technical requirements.

- Watercolour pencils
- [DreamStudio (Stable Diffusion)](https://dreamstudio.ai/)

## Creation Process

This artwork was created by lemur47 using Stable Diffusion.

1. Make a conceptual art with watercolour pencils
2. Send Stable Diffusion the rough sketch with a prompt
3. Review and select the output for a blueprint

## Final Version

![Final version of etheric creation process](art-etheric-creation-process-final.jpeg)

## Concept Design

- [Conceptual design for input to AI](art-etheric-creation-process-concept.jpeg)

### Prompt

> colourful bubbles forming a shape of reality with etheric water, holographic, spiritual, imaginary, highly detailed, conceptual
