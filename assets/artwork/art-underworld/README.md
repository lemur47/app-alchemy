# Artwork - The Underworld

The experience of the underworld or the other world.

This artwork is an abstract explanation of what lemur47 saw, encountered and escaped from during several visits to the otherworldly realms and the underworld in a dream state. This type of dream state is completely different from restructuring and organising past experiences.

- Oil pastels
- [DreamStudio (Stable Diffusion)](https://dreamstudio.ai/)

## Creation Process

This artwork was created by lemur47 using Stable Diffusion as the "AI in the middle". 

1. Make a rough sketch with oil pastels
2. Send Stable Diffusion the rough sketch with a prompt
3. Review and select the output for an artwork

## Final Version

![Enhance - the underworld](./art-underworld-enhance.jpeg)

## Concept and Draft Versions

- [Conceptual design for input to AI](./art-underworld-concept.jpeg)
- [Draft version with the Fantasy Art style](./art-underworld-fantasy-art.jpeg)

*Note: The abstract concept is prepared for the collaboration with the generative AI. With the image and a prompt, the generative AI creates several images accordingly.*

### Prompt
> an otherworldly reptilian behind the ghostly people in a subterranean tunnel with strange plants and red fruits, science fiction, surrealism, cubism, psychedelic, high detail

*Note: Precisely, "an otherworldly reptilian" is NOT correct. It's more like a giant dinosaur-like warrior with many human faces or masks in a tunnel. But to give the AI an understandable prompt, we use the term "reptilian".*
