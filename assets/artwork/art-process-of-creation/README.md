# Artwork - The Process of Creation

The process of creation with the four basic elements on etheric substance.

This creation requires us to work with etheric substance in a higher information density. This density is like a huge resource pool where everything is directly connected and intermingled. Things happen simultaneously, like input and output.

In the near future, with this density and etheric technology, humanity will participate in the terraforming project known as planetary virtualisation.

- Oil pastels
- Watercolour pencils
- [DreamStudio (Stable Diffusion)](https://dreamstudio.ai/)

## Creation Process

This artwork was created by lemur47 using Stable Diffusion as the "AI in the middle". 

1. Make a rough sketch with oil pastels
2. Send Stable Diffusion the rough sketch with a prompt
3. Review and select the output for a blueprint
4. Draw a final painting with watercolour pencils

## Final Version

![Final version with watercolour pencils](art-the-process-of-creation-final.jpg)

## Blueprint and Draft Versions

- [Main blueprint with the Fantasy Art style](art-the-process-of-creation-fantasy-art.jpeg)
- [Draft version with the Normal style](art-the-process-of-creation-normal.jpeg)
- [Draft version with the Anime style](art-the-process-of-creation-anime.jpeg)
- [Draft version with the Neon Punk style](art-the-process-of-creation-neon-punk.jpeg)
- [Draft version with the Digital Art style](art-the-process-of-creation-digital-art.jpeg)

## Rough Sketch for Concept Design

- [Conceptual design for input to AI](art-the-process-of-creation-concept.jpeg)

### Prompt

> otherworldly flower, trees and blue temple, psychedelic, artistic, surrealism, high detail
