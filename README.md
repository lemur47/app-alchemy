# Digital Replica for the Genetic Mind

## Overview

This open-source project and repository aims to share the conceptual solution to how you and I, as humanity, can optimise and innovate our planet and its civilisation from the ground up.

## Content

We're constantly updating this repository with the following content and materials.

### Research Materials and Open Source Works
- [Art-based technical descriptions](./assets/images/)
- [Diagram of system domains and structure](./assets/diagrams/)
- [Sound art as blueprints for unknown technology](./assets/sounds/)
- [Video links for wordless explanations](./assets/movies/)
- [Sound resources for your content creation](./assets/music/)
- [Artwork for your content or collage](./assets/artwork/)
- [Command line tools for your productivity and privacy](./tools/)
- [Text-based explanation of the human mind system](./docs/)
- [Book content - The Secret Portal of Alchemy](./book/)

## Technology

Our main focus is on mind technology. Mind technology is used to manipulate etheric substances, integrate objects (mind modules) and activate electromagnetic information energy on many levels.

Of course, our planetary civilisation and its core (mindset and credo) are built with mind technology. This means that mind technology allows you to innovate the universe, more specifically, this virtual universe.

## Why Mind Technology?

Mind technology has enormous potential to advance not only the human mind system but also our science and technology.

Throughout human history, mind technology has been partially known as magic and alchemy, and the information has also been kept secret from the majority of people. Our mission is to make the secret or esoteric information public as an open system for everyone.

## Why GitLab?

Git technology can be a good analogy to expand our mindset related to space-time.

Interestingly, git technology is very similar to the structure of the genetic mind as part of the human mind system designed and operating within the virtual universe on this planet.

The genetic mind and the virtual universe are relatively free from the rule of linear space-time, so it's worth exploring this further for our collective sovereignty and freedom. So it's pretty obvious why we chose GitLab over GitHub.

## Links

- [Ethical Works](https://www.ethical.works/)
- [Umagick Lab (Japanese only)](https://www.umagick.com/)
- [SoundCloud](https://soundcloud.com/lemur47)

## Donation

- [Buy Me a Coffee](https://buymeacoffee.com/lemur47)
