# Protect Your Privacy with Simple Tools

## How to Remove Gps Metadata from Your Photo
To make it easy for you, we wrote a simple script for both single and bulk removal.
- [Check this script](./rmgps)

### Single removal (with a specific photo)
Run `rmgps FILENAME` in your terminal app. FILENAME should be replaced with the name of your photo.

```
% rmgps IMG_0213.JPG 
Removing GPS metadeta from IMG_0213.JPG
    1 image files updated
```

### Bulk removal (in a specific directory)
Run `rmgps -a` in your terminal app after changing to the directory you want to work in.

```
% rmgps -a
Removing GPS metadeta from IMG_0213.JPG
    1 image files updated
Removing GPS metadeta from IMG_0446.jpg
    1 image files updated
Removing GPS metadeta from IMG_0447.jpg
    1 image files updated
```

To ensure that the metadata has been successfully removed, you can run the following command.

```
% exiftool IMG_0213.JPG |grep -i gps
```

## How to Hide Your Terminal Information When Sharing a Screen
There are several ways to do this, but this is the one we like. Use alias settings.

- [Check this script](./zshrc)

```
lemur47@ethicalworks ~ % hack
[mindhacker] ~ $ <--- share your screen from here
[mindhacker] ~ $ back
lemur47@ethicalworks ~ %
```
