# Tools for Your Daily Work
Sharing the tools we create and use for your privacy and productivity.

If you are a content creator or an author sharing new ideas for our collective sovereignty and freedom, you might want to have very cost-effective solutions and tools to develop and share your content.

In this directory you will find free tools based on open source technology that we always use to save our time. **You can start saving time now** by automating repetitive tasks with these free tools!

We will continue to add low-cost solutions to help you set up your website for the cost of your domain name.

## Categories

- [Productivity](./productivity/)
- [Privacy](./privacy/)

## Requirements
- Linux, BSD or macOS
- Terminal app (Zsh or Bash)
- Package manager such as apt or [Homebew](https://brew.sh/) for macOS
- Python

## Modules
- [ImageMagick](https://imagemagick.org/)
- [ExifTool](https://www.exiftool.org/)

## How to Use
You can clone this repository and copy the commands to your local directory. The directory path can be /usr/local/bin, /opt/local/bin, ~/bin or ~/.bin, etc..
