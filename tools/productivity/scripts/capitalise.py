"""
Capitalise your blog title with APA format

How it works
1. Capitalise the title, then
2. Lowercase the specific words
3. Copy the result to the clipboard

Specific words
- Articles (“a,” “an,” “the”)
- Short conjunctions (e.g., “and,” “as,” “but,” “for,” “if,” “nor,” “or,” “so,” “yet”)
- Short prepositions (e.g., “as,” “at,” “by,” “for,” “in,” “of,” “off,” “on,” “per,” “to,” “up,” “via”)

Reference
https://apastyle.apa.org/style-grammar-guidelines/capitalization/title-case
"""

import sys
import re
import string
import subprocess

def apa_capitalise(text):
    pattern = r'\b(The|A|An|And|As|But|For|If|Nor|Or|So|Yet|At|By|In|Of|Off|On|Per|To|Up|Via)\b'
    text = string.capwords(text)
    words = text.split()
    first = True
    cap_words = []
    for word in words:
        if first:
            word = hyphen_capitalise(word)
            cap_words.append(word)
            first = False
        else:
            word = hyphen_capitalise(word)
            word = re.sub(pattern, lambda m: m.group().lower(), word)
            cap_words.append(word)
    result = ' '.join(cap_words)
    return result

def hyphen_capitalise(text):
    if "-" in text:
        text = text.title()
        # text = re.sub(r"(\w+)-(\w+)", lambda m: f"{m.group(1).capitalize()}-{m.group(2).capitalize()}", text)
    return text

def copy_to_clipboard(text):
    command = "pbcopy"
    process = subprocess.Popen(command, stdin=subprocess.PIPE)
    process.communicate(text.encode("utf-8"))

title = sys.argv[1]
capitalised_title = apa_capitalise(title)
copy_to_clipboard(capitalised_title)
