import sys
from decimal import Decimal, ROUND_HALF_UP

base_freq = float(sys.argv[1])
decimal_places = ".001"
major_scale = (1, 9/8, 5/4, 4/3, 3/2, 5/3, 15/8, 2/1)
minor_scale = (1, 9/8, 6/5, 4/3, 3/2, 8/5, 9/5, 2/1)

print("\nMAJOR SCALE")
for freq in major_scale:
    freq = base_freq * freq
    freq = Decimal(str(freq))
    freq = freq.quantize(Decimal(decimal_places), rounding=ROUND_HALF_UP)
    print(freq)

print("\n\nMINOR SCALE")
for freq in minor_scale:
    freq = base_freq * freq
    freq = Decimal(str(freq))
    freq = freq.quantize(Decimal(decimal_places), rounding=ROUND_HALF_UP)
    print(freq)

print("\n")
